﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileDataExchange.Models
{
    /// <summary>SMS Message Status Model</summary>
    public class MessageStatusModel
    {
        /// <summary>Message Status Description</summary>
        public string Description { get; set; } = "";

        /// <summary>Message Status Success True/False</summary>
        public bool Success { get; set; } = false;
    }
}