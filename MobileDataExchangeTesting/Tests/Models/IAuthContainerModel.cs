﻿using System.Security.Claims;
namespace MobileDataExchange.Models
{
#pragma warning disable CS1591
    public interface IAuthContainerModel
    {
        string SecretKey { get; set; }
        string SecurityAlgorithm { get; set; }
        int ExpireMinutes { get; set; }
        Claim[] Claims { get; set; }
    }
}
