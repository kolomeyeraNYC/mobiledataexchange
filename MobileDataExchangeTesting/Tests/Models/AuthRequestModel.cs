﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Auth Request information</summary>
    [Serializable]
    public class AuthRequestModel
    {
        /// <summary>Default Constructor</summary>
        public AuthRequestModel()
        {

        }
        /// <summary>AuthRequestModel Constructor </summary>
        public AuthRequestModel(PhoneModel phone, string verificationCode, string fcmToken)
        {
            Phone = phone;
            VerificationCode = verificationCode;
            FcmToken = fcmToken;
        }

        /// <summary>Phone information</summary>
        [Required]
        [JsonProperty("phone")]
        public PhoneModel Phone { get; set; }

        /// <summary>SMS verification code</summary>
        [JsonProperty("verificationCode")]
        public string VerificationCode { get; set; }

        /// <summary>Device FCM token</summary>
        [Required]
        [JsonProperty("fcmToken")]
        public string FcmToken { get; set; }
    }
}
