﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Web.Configuration;

namespace MobileDataExchange.Models
{
#pragma warning disable CS1591
    public class JWTContainerModel : IAuthContainerModel
    {
        public int ExpireMinutes { get; set; }
        public string SecretKey { get; set; }
        public string SecurityAlgorithm { get; set; } = SecurityAlgorithms.HmacSha256Signature;

        public Claim[] Claims { get; set; }
    }
}
