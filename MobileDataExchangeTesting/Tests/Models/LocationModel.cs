﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Provides minimal and current application version </summary>
    [Serializable]
    public class LocationModel
    {
        /// <summary>Default Constructor</summary>
        public LocationModel()
        {

        }
        /// <summary>LocationModel Constructor </summary>
        public LocationModel(double lat, double lng)
        {
            Lat = lat;
            Lng = lng;
        }

        /// <summary>Location Latitude</summary>
        [JsonProperty("lat")]
        public double Lat { get; set; }

        /// <summary>Location Longitude</summary>
        [JsonProperty("lng")]
        public double Lng { get; set; }
    }
}
