﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;


namespace MobileDataExchange.Models
{
    /// <summary>Provides phone information </summary>
    [Serializable]
    public class PhoneModel
    {
        /// <summary>Default Constructor</summary>
        public PhoneModel()
        {

        }
        /// <summary>PhoneModel Constructor</summary>
        public PhoneModel(string phone, CountryCodeModel countryCode)
        {
            Phone = phone;
            CountryCode = countryCode;
        }

        /// <summary>Phone number</summary>
        [JsonProperty("phone")]
        public string Phone { get; set; }

        /// <summary>Country code information. Type [CountryCodeModel]</summary>
        [JsonProperty("countryCode")]
        public CountryCodeModel CountryCode { get; set; }
    }
}
