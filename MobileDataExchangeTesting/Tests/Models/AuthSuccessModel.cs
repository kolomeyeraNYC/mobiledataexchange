﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Auth Success Information</summary>
    [Serializable]
    public class AuthSuccessModel
    {
        /// <summary>Default Constructor</summary>
        public AuthSuccessModel()
        {
        }

        /// <summary>AuthSuccessModel Constructor</summary>
        public AuthSuccessModel(string token, ClientModel client)
        {
            Token = token;
            Client = client;
        }

        /// <summary>JWT Token</summary>
        [Required]
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>Client information. [ClientModel]</summary>
        [Required]
        [JsonProperty("client")]
        public ClientModel Client { get; set; }
    }
}
