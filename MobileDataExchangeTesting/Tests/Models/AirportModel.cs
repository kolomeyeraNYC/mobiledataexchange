﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>
    /// Provides Airport information
    /// </summary>
    [Serializable]
    public class AirportModel
    {
        /// <summary>Default Constructor</summary>
        public AirportModel()
        {

        }
        /// <summary>AirportModel Constructor </summary>
        public AirportModel(string iata, string name, string terminal, FlightModel flight)
        {
            this.iata = iata;
            Name = name;
            Terminal = terminal;
            Flight = flight;
        }


       


        /// <summary>iata Code name</summary>
        [JsonProperty("iata")]
        public string iata { get; set; }

        /// <summary>Airport name</summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>Terminal Information </summary>
        [JsonProperty("terminal")]
        public string Terminal { get; set; }

        /// <summary>Flight Information [FlightModel]</summary>
        [JsonProperty("flight")]
        public FlightModel Flight { get; set; }
    }
}
