﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Ride Information </summary>
    [Serializable]
    public class RideModel
    {
        /// <summary>Default Constructor </summary>
        public RideModel()
        {
        }

        /// <summary>RideModel Constructor </summary>
        public RideModel(RouteModel route, long puTime, string vehicleType)
        {
            Route = route;
            PuTime = puTime;
            VehicleType = vehicleType;
        }

        /// <summary>Route information of RouteModel</summary>
        [JsonProperty("route")]
        public RouteModel Route { get; set; }

        /// <summary>PuTime long</summary>
        [JsonProperty("puTime")]
        public long PuTime { get; set; }

        /// <summary>Vehicle Type information (Enum: SEDAN, SUV, LUXURY_SEDAN, WC, MINIVAN)</summary>
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }
    }
}
