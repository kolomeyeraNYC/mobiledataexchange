﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>
    /// Provides Registration information 
    /// </summary>
    [Serializable]

    public class RegistrationModel
    {
        /// <summary>Default Constructor</summary>
        public RegistrationModel()
        {

        }
        /// <summary>Constructor for RegistrationModel</summary>
        public RegistrationModel(bool termAndPolicyAgreed, bool complete)
        {
            TermAndPolicyAgreed = termAndPolicyAgreed;
            FullName = complete;
        }

        /// <summary>Term And Policy Agreed information</summary>
        [JsonProperty("termAndPolicyAgreed")]
        public bool TermAndPolicyAgreed { get; set; }

        /// <summary>FullName information</summary>
        [JsonProperty("fullName")]
        public bool FullName { get; set; }
    }
}
