﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Provides application version information</summary>
    [Serializable]
    public class AppVersionsModel
    {
        /// <summary>Default Constructor</summary>
        public AppVersionsModel()
        {

        }
        /// <summary>CountryCode Constructor </summary>
        public AppVersionsModel(int minimal, int current)
        {
            Minimal = minimal;
            Current = current;
        }

        /// <summary>Minimal application version</summary>
        [JsonProperty("minimal")]
        public int Minimal { get; set; }

        /// <summary>Current application version</summary>
        [JsonProperty("current")]
        public int Current { get; set; }
    }
}
