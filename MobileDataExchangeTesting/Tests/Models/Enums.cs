﻿namespace MobileDataExchange.Models
{
#pragma warning disable CS1591
    /// <summary>VehicleTypeEnum used for ServiceTypeModel.VehicleType converted to string</summary>
    public enum VehicleTypeEnum
    {
        SEDAN=1, //pax= 4 BAGS= 2
        SUV=8,  //pax= 5 BAGS= 3
        LUXURY_SEDAN=2, //pax= 4 BAGS= 2
        WC=17,  //pax= 4 BAGS= 2
        MINIVAN = 3 //pax= 6 BAGS= 2
    }

    /// <summary>AddressTypeEnum be used for AddressModel.Type converted to string</summary>
    public enum AddressTypeEnum
    {
        HOME,
        WORK,
        FAVORITE,
        RECENT,
        SAVED_PLACE,
        AS_DIRECTED
    }
    /// <summary>PlaceTypeEnum </summary>
    public enum PlaceTypeEnum
    {
        IOS,
        ANDROID,
        UNKNOWN
    }

    /// <summary>OsType specifies the User_Agent - the type of device request came from.</summary>
    public enum OsTypeEnum
    {
        IOS,
        ANDROID,
        UNKNOWN
    }
    /// <summary>PaymentStatus status of the payment method.</summary>
    public enum PaymentMethodStatusEnum
    {
        VERIFIED,
        NOT_VERIFIED,
        PENDING,
        EXPIRED
    }
    /// <summary>PaymentType specifies the Payment Type.</summary>

    public enum PaymentTypeEnum
    {
        CARD,
        G_PAY
    }
    /// <summary>BankCardType specifies the Bank Card Type</summary>

    public enum BankCardTypeEnum
    {
        VISA,
        MASTERCARD,
        AMEX,
        DISCOVER,
        DINERS
    }
    public enum FlightMeetingAreasEnum
    {
        INSIDE,
        OUTSIDE,
        PARKING

    }
}
