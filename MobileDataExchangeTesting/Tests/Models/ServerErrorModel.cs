﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>
    /// Server Error Model 
    /// </summary>
    [Serializable]
    public class ServerErrorModel
    {
        /// <summary>Default Constructor</summary>
        public ServerErrorModel()
        {
        }
        /// <summary>ServerErrorModel Constructor</summary>
        public ServerErrorModel(int errorCode, string errorMessage, string errorPayload)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            ErrorPayload = errorPayload;
        }

        /// <summary>Error Code</summary>
        [JsonProperty("errorCode")]
        public int ErrorCode { get; set; }

        /// <summary>Error Message</summary>
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        /// <summary>Error Payload</summary>
        [JsonProperty("errorPayload")]
        public string ErrorPayload { get; set; }
    }
}
