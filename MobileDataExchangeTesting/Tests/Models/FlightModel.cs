﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Flight information</summary>
    [Serializable]
    public class FlightModel
    {
        /// <summary>Default Constructor</summary>
        public FlightModel()
        {

        }
        /// <summary>FlightModel Constructor </summary>
        public FlightModel(string number, string fromCity, string status, long arrivingTime, string meetingArea)
        {
            Number = number;
            FromCity = fromCity;
            Status = status;
            ArrivingTime = arrivingTime;
            MeetingArea = meetingArea;
        }

        /// <summary>Flight number</summary>
        [JsonProperty("number")]
        public string Number { get; set; }

        /// <summary>
        /// From City
        /// </summary>
        [JsonProperty("fromCity")]
        public string FromCity { get; set; }

        /// <summary>Flight status</summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>Arriving time timestamp</summary>
        [JsonProperty("arrivingTime")]
        public long ArrivingTime { get; set; }

        /// <summary>Meeting Area information string (enum, INSIDE, OUTSIDE)</summary>
        [JsonProperty("meetingArea")]
        public string MeetingArea { get; set; }
    }

}
