﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Service Type information </summary>
    [Serializable]
    public class ServiceTypeModel
    {
        /// <summary>Default Constructor</summary>
        public ServiceTypeModel()
        {

        }
        /// <summary>ServiceTypeModel Constructor</summary>
        public ServiceTypeModel(string vehicleType, string vehicleTypeLabel, int pax, int bags, FareModel fare, long eta)
        {
            VehicleType = vehicleType;
            VehicleTypeLabel = vehicleTypeLabel;
            Pax = pax;
            Bags = bags;
            Fare = fare;
            Eta = eta;
        }

        /// <summary>Vehicle Type -  (Enum: SEDAN, SUV, LUXURY_SEDAN, WC, MINIVAN)</summary>
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }

        /// <summary>Country code information. [CountryCodeModel]</summary>
        [JsonProperty("vehicleTypeLabel")]
        public string VehicleTypeLabel { get; set; }

        /// <summary>Number of passangers</summary>
        [JsonProperty("pax")]
        public int Pax { get; set; }

        /// <summary>Number of bags</summary>
        [JsonProperty("bags")]
        public int Bags { get; set; }

        /// <summary>Fare information [FareModel]</summary>
        [JsonProperty("fare")]
        public FareModel Fare { get; set; }
        
        /// <summary>Eta information </summary>
        [JsonProperty("eta")]
        public long Eta { get; set; }
        
    }
}
