﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tests
{
    public partial class ftmCode : Form
    {
        public ftmCode()
        {
            InitializeComponent();
        }

        private void ftmCode_Load(object sender, EventArgs e)
        {

        }

        private void textBoxCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void ftmCode_Activated(object sender, EventArgs e)
        {
            textBoxCode.Focus();
        }

        private void buttonSignout_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxCode.Text.Trim().Length == 0)
            {
                MessageBox.Show("Unable to verify code.\n\nCode is missing...", "Oops",MessageBoxButtons.OK,MessageBoxIcon.Error);
                textBoxCode.Focus();
                return;
            }
            DialogResult = DialogResult.OK;
            Hide();
        }

        private void textBoxCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(null, null);
            }                
        }
    }
}
