﻿namespace Tests
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonVersions = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonCode = new System.Windows.Forms.Button();
            this.buttonVerify = new System.Windows.Forms.Button();
            this.buttonAuth = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonGetSavedPlaces = new System.Windows.Forms.Button();
            this.buttonAddSavedPlace = new System.Windows.Forms.Button();
            this.buttonGetPaymentMethods = new System.Windows.Forms.Button();
            this.buttonServiceAvailability = new System.Windows.Forms.Button();
            this.buttonAgreeTerms = new System.Windows.Forms.Button();
            this.buttonSignout = new System.Windows.Forms.Button();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxToken = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonVersions
            // 
            this.buttonVersions.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonVersions.FlatAppearance.BorderSize = 0;
            this.buttonVersions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonVersions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVersions.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonVersions.ForeColor = System.Drawing.Color.White;
            this.buttonVersions.Location = new System.Drawing.Point(0, 0);
            this.buttonVersions.Name = "buttonVersions";
            this.buttonVersions.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonVersions.Size = new System.Drawing.Size(166, 43);
            this.buttonVersions.TabIndex = 0;
            this.buttonVersions.Text = "Versions";
            this.buttonVersions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonVersions.UseVisualStyleBackColor = true;
            this.buttonVersions.Click += new System.EventHandler(this.buttonVersions_Click);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(166, 88);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(654, 524);
            this.textBox1.TabIndex = 1;
            // 
            // buttonCode
            // 
            this.buttonCode.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonCode.FlatAppearance.BorderSize = 0;
            this.buttonCode.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCode.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCode.ForeColor = System.Drawing.Color.White;
            this.buttonCode.Location = new System.Drawing.Point(0, 43);
            this.buttonCode.Name = "buttonCode";
            this.buttonCode.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonCode.Size = new System.Drawing.Size(166, 43);
            this.buttonCode.TabIndex = 2;
            this.buttonCode.Text = "Get Code";
            this.buttonCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCode.UseVisualStyleBackColor = true;
            this.buttonCode.Click += new System.EventHandler(this.buttonCode_Click);
            // 
            // buttonVerify
            // 
            this.buttonVerify.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonVerify.FlatAppearance.BorderSize = 0;
            this.buttonVerify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVerify.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonVerify.ForeColor = System.Drawing.Color.White;
            this.buttonVerify.Location = new System.Drawing.Point(0, 86);
            this.buttonVerify.Name = "buttonVerify";
            this.buttonVerify.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonVerify.Size = new System.Drawing.Size(166, 43);
            this.buttonVerify.TabIndex = 3;
            this.buttonVerify.Text = "Verify Code";
            this.buttonVerify.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonVerify.UseVisualStyleBackColor = true;
            this.buttonVerify.Click += new System.EventHandler(this.buttonVerify_Click);
            // 
            // buttonAuth
            // 
            this.buttonAuth.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAuth.FlatAppearance.BorderSize = 0;
            this.buttonAuth.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonAuth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAuth.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAuth.ForeColor = System.Drawing.Color.White;
            this.buttonAuth.Location = new System.Drawing.Point(0, 129);
            this.buttonAuth.Name = "buttonAuth";
            this.buttonAuth.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonAuth.Size = new System.Drawing.Size(166, 43);
            this.buttonAuth.TabIndex = 5;
            this.buttonAuth.Text = "Auth";
            this.buttonAuth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAuth.UseVisualStyleBackColor = true;
            this.buttonAuth.Click += new System.EventHandler(this.buttonAuth_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(56)))), ((int)(((byte)(79)))));
            this.panel1.Controls.Add(this.buttonGetSavedPlaces);
            this.panel1.Controls.Add(this.buttonAddSavedPlace);
            this.panel1.Controls.Add(this.buttonGetPaymentMethods);
            this.panel1.Controls.Add(this.buttonServiceAvailability);
            this.panel1.Controls.Add(this.buttonAgreeTerms);
            this.panel1.Controls.Add(this.buttonSignout);
            this.panel1.Controls.Add(this.textBoxPhone);
            this.panel1.Controls.Add(this.buttonAuth);
            this.panel1.Controls.Add(this.buttonVerify);
            this.panel1.Controls.Add(this.buttonCode);
            this.panel1.Controls.Add(this.buttonVersions);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(166, 579);
            this.panel1.TabIndex = 10;
            // 
            // buttonGetSavedPlaces
            // 
            this.buttonGetSavedPlaces.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonGetSavedPlaces.FlatAppearance.BorderSize = 0;
            this.buttonGetSavedPlaces.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonGetSavedPlaces.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGetSavedPlaces.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonGetSavedPlaces.ForeColor = System.Drawing.Color.White;
            this.buttonGetSavedPlaces.Location = new System.Drawing.Point(0, 387);
            this.buttonGetSavedPlaces.Name = "buttonGetSavedPlaces";
            this.buttonGetSavedPlaces.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonGetSavedPlaces.Size = new System.Drawing.Size(166, 43);
            this.buttonGetSavedPlaces.TabIndex = 16;
            this.buttonGetSavedPlaces.Text = "Get Saved Places";
            this.buttonGetSavedPlaces.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGetSavedPlaces.UseVisualStyleBackColor = true;
            this.buttonGetSavedPlaces.Click += new System.EventHandler(this.buttonGetSavedPlaces_Click);
            // 
            // buttonAddSavedPlace
            // 
            this.buttonAddSavedPlace.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAddSavedPlace.FlatAppearance.BorderSize = 0;
            this.buttonAddSavedPlace.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonAddSavedPlace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddSavedPlace.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddSavedPlace.ForeColor = System.Drawing.Color.White;
            this.buttonAddSavedPlace.Location = new System.Drawing.Point(0, 344);
            this.buttonAddSavedPlace.Name = "buttonAddSavedPlace";
            this.buttonAddSavedPlace.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonAddSavedPlace.Size = new System.Drawing.Size(166, 43);
            this.buttonAddSavedPlace.TabIndex = 17;
            this.buttonAddSavedPlace.Text = "Add Saved Place";
            this.buttonAddSavedPlace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddSavedPlace.UseVisualStyleBackColor = true;
            this.buttonAddSavedPlace.Click += new System.EventHandler(this.buttonAddSavedPlace_Click);
            // 
            // buttonGetPaymentMethods
            // 
            this.buttonGetPaymentMethods.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonGetPaymentMethods.FlatAppearance.BorderSize = 0;
            this.buttonGetPaymentMethods.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonGetPaymentMethods.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGetPaymentMethods.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonGetPaymentMethods.ForeColor = System.Drawing.Color.White;
            this.buttonGetPaymentMethods.Location = new System.Drawing.Point(0, 301);
            this.buttonGetPaymentMethods.Name = "buttonGetPaymentMethods";
            this.buttonGetPaymentMethods.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonGetPaymentMethods.Size = new System.Drawing.Size(166, 43);
            this.buttonGetPaymentMethods.TabIndex = 15;
            this.buttonGetPaymentMethods.Text = "Get Payment Methods";
            this.buttonGetPaymentMethods.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGetPaymentMethods.UseVisualStyleBackColor = true;
            this.buttonGetPaymentMethods.Click += new System.EventHandler(this.buttonGetPaymentMethods_Click);
            // 
            // buttonServiceAvailability
            // 
            this.buttonServiceAvailability.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonServiceAvailability.FlatAppearance.BorderSize = 0;
            this.buttonServiceAvailability.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonServiceAvailability.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonServiceAvailability.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonServiceAvailability.ForeColor = System.Drawing.Color.White;
            this.buttonServiceAvailability.Location = new System.Drawing.Point(0, 258);
            this.buttonServiceAvailability.Name = "buttonServiceAvailability";
            this.buttonServiceAvailability.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonServiceAvailability.Size = new System.Drawing.Size(166, 43);
            this.buttonServiceAvailability.TabIndex = 14;
            this.buttonServiceAvailability.Text = "Service Availability";
            this.buttonServiceAvailability.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonServiceAvailability.UseVisualStyleBackColor = true;
            this.buttonServiceAvailability.Click += new System.EventHandler(this.buttonServiceAvailability_Click);
            // 
            // buttonAgreeTerms
            // 
            this.buttonAgreeTerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAgreeTerms.FlatAppearance.BorderSize = 0;
            this.buttonAgreeTerms.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonAgreeTerms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAgreeTerms.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAgreeTerms.ForeColor = System.Drawing.Color.White;
            this.buttonAgreeTerms.Location = new System.Drawing.Point(0, 215);
            this.buttonAgreeTerms.Name = "buttonAgreeTerms";
            this.buttonAgreeTerms.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonAgreeTerms.Size = new System.Drawing.Size(166, 43);
            this.buttonAgreeTerms.TabIndex = 11;
            this.buttonAgreeTerms.Text = "AgreeTerms";
            this.buttonAgreeTerms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAgreeTerms.UseVisualStyleBackColor = true;
            this.buttonAgreeTerms.Click += new System.EventHandler(this.buttonAgreeTerms_Click);
            // 
            // buttonSignout
            // 
            this.buttonSignout.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSignout.FlatAppearance.BorderSize = 0;
            this.buttonSignout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.buttonSignout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSignout.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSignout.ForeColor = System.Drawing.Color.White;
            this.buttonSignout.Location = new System.Drawing.Point(0, 172);
            this.buttonSignout.Name = "buttonSignout";
            this.buttonSignout.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.buttonSignout.Size = new System.Drawing.Size(166, 43);
            this.buttonSignout.TabIndex = 10;
            this.buttonSignout.Text = "Signout";
            this.buttonSignout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSignout.UseVisualStyleBackColor = true;
            this.buttonSignout.Click += new System.EventHandler(this.buttonSignout_Click);
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(56)))), ((int)(((byte)(79)))));
            this.textBoxPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPhone.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxPhone.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.textBoxPhone.ForeColor = System.Drawing.Color.White;
            this.textBoxPhone.Location = new System.Drawing.Point(0, 545);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(166, 18);
            this.textBoxPhone.TabIndex = 12;
            this.textBoxPhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxPhone.Enter += new System.EventHandler(this.textBoxPhone_Enter);
            this.textBoxPhone.Leave += new System.EventHandler(this.textBoxPhone_Leave);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 563);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(166, 16);
            this.panel3.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(127)))));
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(820, 33);
            this.panel2.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(791, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 33);
            this.button1.TabIndex = 13;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 0, 50, 0);
            this.label2.Size = new System.Drawing.Size(820, 33);
            this.label2.TabIndex = 12;
            this.label2.Text = "CTG Mobile Data Exchange API Testing";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label2_MouseDown);
            // 
            // textBoxToken
            // 
            this.textBoxToken.BackColor = System.Drawing.Color.Gainsboro;
            this.textBoxToken.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxToken.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxToken.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(56)))), ((int)(((byte)(79)))));
            this.textBoxToken.Location = new System.Drawing.Point(166, 33);
            this.textBoxToken.Name = "textBoxToken";
            this.textBoxToken.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.textBoxToken.Size = new System.Drawing.Size(654, 55);
            this.textBoxToken.TabIndex = 10;
            this.textBoxToken.Text = "No Token";
            this.textBoxToken.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(820, 612);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBoxToken);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonVersions;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonCode;
        private System.Windows.Forms.Button buttonVerify;
        private System.Windows.Forms.Button buttonAuth;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonSignout;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label textBoxToken;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonAgreeTerms;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonServiceAvailability;
        private System.Windows.Forms.Button buttonGetPaymentMethods;
        private System.Windows.Forms.Button buttonGetSavedPlaces;
        private System.Windows.Forms.Button buttonAddSavedPlace;
    }
}

