﻿using MobileDataExchange.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Tests
{
    public partial class Form1 : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBoxPhone.Text = Properties.Settings.Default.phoneNumber;
        }

        private void buttonVersions_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/versions");

                client.UserAgent = "IOS";
                var request = new RestRequest(Method.GET);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("device-id", "1234");

                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void buttonCode_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                PhoneModel phone = new PhoneModel();
                phone.Phone = textBoxPhone.Text;
                phone.CountryCode = new CountryCodeModel();
                phone.CountryCode.DialCode = "";
                phone.CountryCode.IsoCode = "US";
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/auth/code");
                //string header = String.Format("BASIC {0}", System.Convert.ToBase64String(concatenated));
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.POST);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                // request.AddHeader("authorization", "Bearer " + "your token key");
                //request.AddHeader("authorization", header);
                request.AddParameter("application/json", JsonConvert.SerializeObject(phone), ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void buttonVerify_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh();
                textBox1.Focus();
                ftmCode frm = new ftmCode();
                if (frm.ShowDialog(this) != DialogResult.OK)
                    return;
                string code = frm.textBoxCode.Text;
                frm.Dispose();
                AuthRequestModel authrequest = new AuthRequestModel();
                authrequest.FcmToken = "123456789";
                authrequest.Phone = new PhoneModel();
                authrequest.Phone.Phone = textBoxPhone.Text;
                authrequest.Phone.CountryCode = new CountryCodeModel();
                authrequest.Phone.CountryCode.Id = 1;
                authrequest.Phone.CountryCode.IsoCode = "US";
                authrequest.VerificationCode = code;
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/auth/phone/verify");
                //string header = String.Format("BASIC {0}", System.Convert.ToBase64String(concatenated));
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.POST);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                // request.AddHeader("authorization", "Bearer " + "your token key");
                //request.AddHeader("authorization", header);
                request.AddParameter("application/json", JsonConvert.SerializeObject(authrequest), ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
                AuthSuccessModel ret = JsonConvert.DeserializeObject<AuthSuccessModel>(response.Content);
                textBoxToken.Text = ret.Token;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void buttonAuth_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                AuthRequestModel authrequest = new AuthRequestModel();
                authrequest.FcmToken = "123456789";
                authrequest.Phone = new PhoneModel();
                authrequest.Phone.Phone = textBoxPhone.Text;
                authrequest.Phone.CountryCode = new CountryCodeModel();
                authrequest.Phone.CountryCode.Id = 1;
                authrequest.Phone.CountryCode.IsoCode = "US";
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/auth");
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.POST);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + textBoxToken.Text);
                request.AddParameter("application/json", JsonConvert.SerializeObject(authrequest), ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
                AuthSuccessModel ret = JsonConvert.DeserializeObject<AuthSuccessModel>(response.Content);
                textBoxToken.Text = ret.Token;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void buttonSignout_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/auth/signout");
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.POST);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + textBoxToken.Text);
                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
                textBoxToken.Text = "";
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAgreeTerms_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/profile/agree-terms");
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.POST);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + textBoxToken.Text);
                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (textBoxPhone.Text.Length >0)
            { 
            Properties.Settings.Default.phoneNumber= textBoxPhone.Text;
            Properties.Settings.Default.Save();
            }
        }

        private void textBoxPhone_Enter(object sender, EventArgs e)
        {
            textBoxPhone.BackColor = Color.FromArgb(81, 96, 119);
        }

        private void textBoxPhone_Leave(object sender, EventArgs e)
        {
            textBoxPhone.BackColor = Color.FromArgb(41, 56, 79);
        }

        private void buttonServiceAvailability_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/service/availability");
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.GET);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + textBoxToken.Text);
                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void buttonGetPaymentMethods_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/payment-methods");
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.GET);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + textBoxToken.Text);
                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void buttonGetSavedPlaces_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/places/saved");
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.GET);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + textBoxToken.Text);
                IRestResponse response = client.Execute(request);
                textBox1.Text = (int)response.StatusCode + Environment.NewLine + response.Content;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void buttonAddSavedPlace_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ""; textBox1.Refresh(); textBox1.Focus();
                PlaceModel place = new PlaceModel();
                place.Type = "HOME";
                place.UserLabel = "My New York home";
                place.PlaceType = "ADDRESS";
                place.Name = "Home Address";
                place.Point = new LocationModel(40.579480, -73.970140);
                place.Address = new AddressModel("444", "Neptune Avenue", "Brooklyn", "NY", "11224", "Kings", "US", new LocationModel(40.579480, -73.970140));
                place.Airport = null;
                var client = new RestClient(Properties.Settings.Default.ApiURL + "/api/mobile/passenger/v1/places/saved");
                client.UserAgent = "IOS";
                var request = new RestRequest(Method.PUT);

                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + textBoxToken.Text);
                request.AddParameter("application/json", JsonConvert.SerializeObject(place), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                textBox1.Text = ((int)response.StatusCode) + Environment.NewLine + response.Content;
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }
    }
}
