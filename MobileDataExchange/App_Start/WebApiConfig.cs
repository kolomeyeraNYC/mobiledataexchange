﻿using System.Web.Http;
using MobileDataExchange.Filters;
using Newtonsoft.Json;

namespace MobileDataExchange
{
#pragma warning disable CS1591
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new ValidateActionPayloadAttribute());
            //config.Filters.Add(new ValidateTokenAttribute());
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings();
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
