﻿using System.Web.Mvc;

namespace MobileDataExchange
{
#pragma warning disable CS1591
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
