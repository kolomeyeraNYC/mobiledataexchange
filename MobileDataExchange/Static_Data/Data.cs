﻿using MobileDataExchange.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileDataExchange.Static_Objects
{
#pragma warning disable CS1591
    public static class StaticData
    {
        public static List<CountryCodeModel> GetCountryCodes()
        {
            List<CountryCodeModel> CountryCodes = JsonConvert.DeserializeObject<List<CountryCodeModel>>(Properties.Resources.PhoneCountryCodesJson);
            return CountryCodes;
        }
    }
}