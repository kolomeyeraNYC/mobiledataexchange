﻿using System.Web.Mvc;

namespace MobileDataExchange.Controllers
{
#pragma warning disable CS1591
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "CTG Mobile Data Exchange";

            return View();
        }
    }
}
