﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MobileDataExchange.Common;
using MobileDataExchange.Filters;
using MobileDataExchange.Models;
using MobileDataExchange.DAL;

namespace MobileDataExchange.Controllers
{
    /// <summary>Mobile Data Exchange Controller supports comunication between mobile device and server</summary>
    //[AuthorizationFilter()]
    [Log]
    public class MobileDataExchangeController : ApiController
    {
        private readonly Repository repository = new Repository();
        /// <summary>Returns minimum and current application versions</summary>
        /// <returns>AppVersionsModel</returns>
        [HttpGet]
        [ActionName("versions")]
        [Route("api/mobile/passenger/versions")]
        public AppVersionsModel GetVersions()
        {
            AppVersionsModel vm = repository.GetVersions(Request); 
            return vm;
        }

        /// <summary>Request to send a confirmation code</summary>
        /// <payload>phone type PhoneModel</payload>
        [HttpPost]
        [ActionName("code")]
        [Route("api/mobile/passenger/v1/auth/code")]
        [ValidateActionPayload]
        public void Code(PhoneModel phone)
        {
            repository.RequestVerificationCode(phone, Request);
        }

        /// <summary>Request to verify Authrequest</summary>
        /// <payload>authrequest type AuthRequestModel</payload>
        /// <returns>AuthSuccessModel</returns>
        [HttpPost]
        [ActionName("verify")]
        [Route("api/mobile/passenger/v1/auth/phone/verify")]
        [ValidateActionPayload]
        public AuthSuccessModel Verify(AuthRequestModel authrequest)
        {
            AuthSuccessModel sm = repository.ValidateVerificationCode(authrequest, Request);
            return sm;
        }

        /// <summary>Request to authorize request</summary>
        /// <payload>authrequest type AuthRequestModel</payload>
        /// <returns>AuthSuccessModel</returns>
        [HttpPost]
        [ActionName("auth")]
        [Route("api/mobile/passenger/v1/auth")]
        [ValidateToken]
        [ValidateActionPayload]
        public AuthSuccessModel Auth(AuthRequestModel authrequest)
        {
            AuthSuccessModel sm = repository.Authorize(authrequest, Request);
            return sm;
        }

        /// <summary>Request to sign out / remove token from client profile</summary>
        [HttpPost]
        [ActionName("signout")]
        [Route("api/mobile/passenger/v1/auth/signout")]
        [ValidateToken]
        public void Signout()
        {
            repository.Signout(Request);
        }
        /// <summary>Agree Terms and Conditions</summary>
        /// <returns>Registration object</returns>
        [HttpPost]
        [ActionName("agree-terms")]
        [Route("api/mobile/passenger/v1/profile/agree-terms")]
        [ValidateToken]
        public RegistrationModel AgreeTerms()
        {
            RegistrationModel rm = repository.AgreeTerms(Request);
            return rm;
        }
        /// <summary>Get available service types List of ServiceType objects</summary>
        /// <returns>List of Client objects </returns>
        [HttpGet]
        [ActionName("serviceavailability")]
        [Route("api/mobile/passenger/v1/service/availability")]
        [ValidateToken]
        public List<ServiceTypeModel> ServiceAvailability()
        {
            List<ServiceTypeModel> st = repository.ServiceAvailability(Request);
            return st;
           
        }
        /// <summary>Update client profile</summary>
        /// <payload>client type ClientModel</payload>
        /// <returns>Client object</returns>
        [HttpPatch]
        [ActionName("profile")]
        [Route("api/mobile/passenger/v1/profile")]
        [ValidateActionPayload]
        [ValidateToken]
        public ClientModel UpdateProfile(ClientModel client)
        {
            ClientModel return_client = repository.UpdateProfile(client, Request);
            return return_client;
        }


        /// <summary>Get profile saved payment methods</summary>
        /// <returns>List of PaymentMethod objects</returns>
        [HttpGet]
        [ActionName("getpaymentmethods")]
        [Route("api/mobile/passenger/v1/payment-methods")]
        [ValidateToken]
        public List<PaymentMethodModel> GetPaymentMethods()
        {
            List<PaymentMethodModel> pmlst = repository.GetPaymentMethods(Request);
            return pmlst;
        }

        /// <summary>Add profile payment method</summary>
        /// <payload>paymentmethod type PaymentMethod object</payload>
        [HttpPut]
        [ActionName("addpaymentmethods")]
        [Route("api/mobile/passenger/v1/payment-methods")]
        [ValidateToken]
        [ValidateActionPayload]
        public void AddPaymentMethods(PaymentMethodModel paymentmethod)
        {
            //????? PARAMETER PaymentMethodModel paymentmethod ????
            repository.AddPaymentMethod(paymentmethod, Request);
        }

        /// <summary>Delete / Deactivate profile payment method</summary>
        /// <parameter>Token type string</parameter>
        [HttpDelete]
        [ActionName("deletepaymentmethod")]
        [Route("api/mobile/passenger/v1/payment-methods/{token}")]
        [ValidateActionPayload]
        [ValidateToken]
        public void DeletePaymentMethods(string token)
        {
            repository.DeletePaymentMethod(token, Request);
        }

        /// <summary>Get profile saved places</summary>
        /// <returns>List of profile PlaceModel objects</returns>
        [HttpGet]
        [ActionName("getsavedplaces")]
        [Route("api/mobile/passenger/v1/places/saved")]
        [ValidateToken]
        public List<PlaceModel> GetSavedPlaces()
        {
            List<PlaceModel> lstpl = repository.GetSavedPlaces(Request);
            return lstpl;
        }
        /// <summary>Add saved place</summary>
        /// <payload>place type PlaceModel</payload>
        /// <returns>new created place id type int</returns>
        [HttpPut]
        [ActionName("addsavedplace")]
        [Route("api/mobile/passenger/v1/places/saved")]
        [ValidateActionPayload]
        [ValidateToken]
        public int AddSavedPlace(PlaceModel place)
        {
            int placeId = repository.AddSavedPlace(place, Request);
            return placeId;
        }

        /// <summary>Delete / Deactivate saved place by inline [id] </summary>
        [HttpDelete]
        [ActionName("deletesavedplace")]
        [Route("api/mobile/passenger/v1/places/saved/{id}")]
        [ValidateToken]
        public void DeleteSavedPlace(int id)
        {
            repository.DeleteSavedPlace(id, Request);
           
        }
        /// <summary>Get recent places list</summary>
        /// <returns>List PlaceModel objects</returns>
        [HttpGet]
        [ActionName("getrecentplaces")]
        [Route("api/mobile/passenger/v1/places/recent")]
        [ValidateToken]
        public List<PlaceModel> GetRecentPlaces()
        {
            List<PlaceModel> lstpl = new List<PlaceModel>();
            return lstpl;
        }



    }
}
