﻿using log4net;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace MobileDataExchange.DAL
{
#pragma warning disable CS1591
    public delegate void Error(string source, Exception ex);

    public delegate void Retry(int num);

    public class DataProviderBak
    {
        public event Error ErroHandler;

        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private bool disposing;

        public string DataSource { get; private set; }

        public string InitialCatalog { get; private set; }

        public string UserID { get; private set; }

        public string Password { get; private set; }

        public string FailoverPartner { get; private set; }

        public string ConnectionString { get; private set; }

        public void Dispose()
        {
            if (disposing)
            {
                return;
            }
            disposing = true;
        }

        public DataProviderBak()
        {
            ConnectionString = WebConfigurationManager.AppSettings["ConnectionString"];
            var builder = new SqlConnectionStringBuilder
            {
                ConnectionString = ConnectionString
            };
            FailoverPartner = builder.FailoverPartner;
            DataSource = builder.DataSource;
            InitialCatalog = builder.InitialCatalog;
            UserID = builder.UserID;
            Password = builder.Password;
        }

        public DataProviderBak Clone()
        {
            return new DataProviderBak();
        }

        public bool ValidateConnection()
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }

            using (var cnn = new SqlConnection())
            {
                cnn.ConnectionString = ConnectionString.Replace(";Connect Timeout=60", ";Connection Timeout=10"); //+ ";Connection Timeout=10";
                try
                {
                    cnn.Open();
                    if (cnn.State == ConnectionState.Open)
                    {
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    log.Debug("DataProvider.ValidateConnection", ex);
                    ErroHandler?.Invoke("DataProvider.ValidateApplication", ex);
                    return false;
                }
            }
        }

        public async Task<bool> ValidateConnectionAsync()
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }

            bool valid = await Task.Run(() => ValidateConnection()).ConfigureAwait(false);
            return valid;
        }

        public SqlConnection GetConnection()
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            var cnn = new SqlConnection { ConnectionString = ConnectionString };

            try
            {
                cnn.Open();
                return cnn;
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetConnection", ex);
                ErroHandler?.Invoke("DataProvider.GetConnection", ex);

                return null;
            }
        }

        public DataSet GetDataSet(string sql, SqlConnection cnn)
        {
            DataSet lDs;
            try
            {
                if (cnn.State != ConnectionState.Open)
                {
                    cnn.Open();
                }
                lDs = new DataSet();
                var adapter = new SqlDataAdapter { SelectCommand = new SqlCommand(sql, cnn) };
                adapter.Fill(lDs);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataSet", ex);
                ErroHandler?.Invoke("DataProvider.GetDataSet", ex);

                return null;
            }
            return lDs;
        }

        public DataTable GetDataTable(string tableName)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            var DT = new DataTable();
            var adapter = new SqlDataAdapter();
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                string sql = "SELECT * FROM [" + tableName + "]";
                adapter.SelectCommand = new SqlCommand(sql, cnn);
                adapter.Fill(DT);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataSet", ex);
                ErroHandler?.Invoke("DataProvider.GetDataSet", ex);
                return null;
            }
            return DT;
        }

        public DataTable GetSQLDataTable(string SQLQuery)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            var DT = new DataTable();
            var adapter = new SqlDataAdapter();
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                adapter.SelectCommand = new SqlCommand(SQLQuery, cnn);
                adapter.Fill(DT);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetSQLDataTable", ex);
                ErroHandler?.Invoke("DataProvider.GetSQLDataTable", ex);
                return null;
            }
            return DT;
        }

        public DataSet GetDataSet(string sql, string tableName, SqlConnection cnn)
        {
            DataSet lDs;
            try
            {
                if (cnn.State != ConnectionState.Open)
                {
                    cnn.Open();
                    //cnn.Open();
                }
                lDs = new DataSet();
                var adapter = new SqlDataAdapter { SelectCommand = new SqlCommand(sql, cnn) };
                adapter.Fill(lDs, tableName);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataSet", ex);
                ErroHandler?.Invoke("DataProvider.GetDataSet", ex);

                return null;
            }
            return lDs;
        }

        public DataSet GetDataSet(string sql)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            DataSet lDs;
            var adapter = new SqlDataAdapter();
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                //cnn.Open();
                lDs = new DataSet();
                adapter.SelectCommand = new SqlCommand(sql, cnn);
                adapter.Fill(lDs);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataSet", ex);
                ErroHandler?.Invoke("DataProvider.GetDataSet", ex);
                return null;
            }
            return lDs;
        }

        public DataSet GetDataSet(string sql, string tableName)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            DataSet lDs;
            var adapter = new SqlDataAdapter();
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                //cnn.Open();
                lDs = new DataSet();
                adapter.SelectCommand = new SqlCommand(sql, cnn);
                adapter.Fill(lDs, tableName);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataSet", ex);
                ErroHandler?.Invoke("DataProvider.GetDataSet", ex);
                return null;
            }
            return lDs;
        }

        public bool BulkCopy(SqlDataReader reader, string tableName)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                using (var bulkCopy = new SqlBulkCopy(cnn))
                {
                    bulkCopy.BulkCopyTimeout = 60;
                    bulkCopy.BatchSize = 1000;
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.WriteToServer(reader);
                }
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.DataReaderBulkCopy", ex);
                ErroHandler?.Invoke("DataProvider.DataReaderBulkCopy", ex);
                return false;
            }
            return true;
        }

        public void BulkCopy(OleDbDataReader reader, string tableName)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return;
            }
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                using (var bulkCopy = new SqlBulkCopy(cnn))
                {
                    bulkCopy.BulkCopyTimeout = 60;
                    bulkCopy.BatchSize = 10000;
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.WriteToServer(reader);
                }
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.DataReaderBulkCopy", ex);
                ErroHandler?.Invoke("DataProvider.DataReaderBulkCopy", ex);
            }
        }

        public bool BulkCopy(DataTable dataTable, string tableName)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                using (var bulkCopy = new SqlBulkCopy(cnn))
                {
                    bulkCopy.BulkCopyTimeout = 60;
                    bulkCopy.BatchSize = 1000;
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.WriteToServer(dataTable);
                }
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.DataTableBulkCopy", ex);
                ErroHandler?.Invoke("DataProvider.DataTableBulkCopy", ex);
                return false;
            }
            return true;
        }

        public SqlDataAdapter GetSQLDataUdapter(string sql)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            try
            {
                var cnn = new SqlConnection(ConnectionString);
                cnn.Open();
                return new SqlDataAdapter(sql, cnn);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetSQLDataUdapter", ex);
                ErroHandler?.Invoke("DataProvider.GetSQLDataUdapter", ex);
                return null;
            }
        }

        public SqlCommandBuilder GetSqlCommandBuilder(SqlDataAdapter DA)
        {
            try
            {
                return new SqlCommandBuilder(DA);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetSqlCommandBuilder", ex);
                ErroHandler?.Invoke("DataProvider.GetSqlCommandBuilder", ex);
                return null;
            }
        }

        public SqlDataReader GetDataReader(string sql)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            SqlDataReader reader;
            var cnn = new SqlConnection(ConnectionString);
            var cmd = new SqlCommand(sql, cnn);
            try
            {
                cnn.Open();
                //cnn.Open();
                cmd.CommandTimeout = 100;
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataReader", ex);
                ErroHandler?.Invoke("DataProvider.GetDataReader", ex);
                return null;
            }
            return reader;
        }

        public SqlDataReader GetDataReader(SqlConnection cnn, string sql)
        {
            if (cnn.State != ConnectionState.Open)
            {
                return null;
            }
            SqlDataReader reader;
            var cmd = new SqlCommand(sql, cnn);
            try
            {
                cmd.CommandTimeout = 100;
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataReader", ex);
                ErroHandler?.Invoke("DataProvider.GetDataReader", ex);
                return null;
            }
            return reader;
        }

        public SqlDataReader GetDataReader(SqlCommand cmd)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return null;
            }
            SqlDataReader reader;
            var cnn = new SqlConnection(ConnectionString);
            try
            {
                cnn.Open();
                //cnn.Open();
                cmd.CommandTimeout = 100;
                cmd.Connection = cnn;
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.GetDataReader", ex);
                ErroHandler?.Invoke("DataProvider.GetDataReader", ex);
                return null;
            }
            return reader;
        }

        public bool GetSingleBool(string sql)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }
            using (var cnn = new SqlConnection(ConnectionString))
            {
                try
                {
                    cnn.Open();
                    //cnn.Open();
                }
                catch (Exception ex)
                {
                    log.Debug("GetSingleBool", ex);
                    return false;
                }
                using (var cmd = new SqlCommand(sql, cnn))
                {
                    try
                    {
                        var ret = cmd.ExecuteScalar();

                        return ret.ToSafeBool();
                    }
                    catch (Exception ex)
                    {
                        log.Debug("DataProvider.GetSingleBool", ex);
                        ErroHandler?.Invoke("DataProvider.GetSingleBool", ex);
                        return false;
                    }
                }
            }
        }

        public double GetSingleValue(string sql)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return 0;
            }
            using (var cnn = new SqlConnection(ConnectionString))
            {
                try
                {
                    cnn.Open();
                    //cnn.Open();
                }
                catch (Exception ex)
                {
                    log.Debug("GetSingleValue", ex);
                    return 0;
                }
                using (var cmd = new SqlCommand(sql, cnn))
                {
                    try
                    {
                        var ret = cmd.ExecuteScalar();
                        if (((!ReferenceEquals(ret, DBNull.Value))) & ((ret != null)))
                        {
                            if (double.TryParse(ret.ToString(), out double functionReturnValue))
                            {
                                return functionReturnValue;
                            }
                            return 0;
                        }
                        return 0;
                    }
                    catch (Exception ex)
                    {
                        log.Debug("DataProvider.GetSingleValue", ex);
                        ErroHandler?.Invoke("DataProvider.GetSingleValue", ex);
                        return 0;
                    }
                }
            }
        }

        public string GetSingleString(string sql)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return "";
            }
            using (var cnn = new SqlConnection(ConnectionString))
            {
                try
                {
                    cnn.Open();
                    //cnn.Open();
                }
                catch (Exception ex)
                {
                    log.Debug("GetSingleString", ex);
                    return "";
                }
                using (var cmd = new SqlCommand(sql, cnn))
                {
                    try
                    {
                        var ret = cmd.ExecuteScalar();
                        if (((!ReferenceEquals(ret, DBNull.Value))) & ((ret != null)))
                        {
                            return ret.ToString();
                        }
                        return "";
                    }
                    catch (Exception ex)
                    {
                        log.Debug("DataProvider.GetSingleString", ex);
                        ErroHandler?.Invoke("DataProvider.GetSingleString", ex);
                        return "";
                    }
                }
            }
        }
        public bool UpdateImage(string sql, int document_id, Byte[] arrBytes)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }
            using (var cnn = new SqlConnection(ConnectionString))
            {
                try
                {
                    cnn.Open();
                    //cnn.Open();
                }
                catch (Exception ex)
                {
                    log.Debug("DataProvider.UpdateImage", ex);
                    return false;
                }
                using (var cmd = new SqlCommand(sql, cnn))
                {
                    try
                    {
                        cmd.Parameters.Add("@document_id", SqlDbType.Int).Value = document_id;
                        cmd.Parameters.Add("@data", SqlDbType.VarBinary, arrBytes.Length).Value = arrBytes;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Debug("DataProvider.UpdateImage", ex);
                        ErroHandler?.Invoke("DataProvider.InsertImage", ex);
                        return false;
                    }
                }
            }
        }
        public bool UpdateData(SqlCommand cmd)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }
            using (var cnn = new SqlConnection(ConnectionString))
            {
                try
                {
                    cnn.Open();
                    //cnn.Open();
                }
                catch (Exception ex)
                {
                    log.Debug("UpdateData", ex);
                    return false;
                }
                try
                {
                    cmd.Connection = cnn;
                    cmd.CommandTimeout = 60;
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    log.Debug("DataProvider.UpdateData", ex);
                    ErroHandler?.Invoke("DataProvider.UpdateData", ex);
                    return false;
                }
            }
        }

        public bool UpdateData(SqlCommand cmd, SqlTransaction trans)
        {
            try
            {
                cmd.CommandTimeout = 60;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.UpdateData", ex);
                ErroHandler?.Invoke("DataProvider.UpdateData", ex);
                return false;
            }
        }

        /// <summary>
        ///     Updates the data.
        /// </summary>
        /// <param name="sql">The SQL query statement.</param>
        /// <param name="commandTimeOut">The SQL query Command TimeOut.</param>
        /// <returns>True if successful</returns>
        public bool UpdateData(string sql, int commandTimeOut = 60)
        {
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }
            using (var cnn = new SqlConnection(ConnectionString))
            {
                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    log.Debug("UpdateData", ex);
                    return false;
                }

                using (var cmd = new SqlCommand(sql, cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = commandTimeOut;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Debug("DataProvider.UpdateData", ex);
                        log.Debug("ERROR SQL: " + sql);
                        ErroHandler?.Invoke("DataProvider.UpdateData", ex);
                        return false;
                    }
                }
            }
        }

        /// <summary>
        ///     Updates the data.
        /// </summary>
        /// <param name="sql">The SQL query statement.</param>
        /// <param name="cnn">The SQL Connection.</param>
        /// <param name="tran">The SQL Transaction.</param>
        /// <param name="throwException">Throw Exception</param>
        /// <returns>True if successful</returns>
        public bool UpdateData(string sql, SqlConnection cnn, SqlTransaction tran, bool throwException = false)
        {
            try
            {
                using (var cmd = new SqlCommand(sql, cnn, tran))
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Debug("DataProvider.UpdateData", ex);
                log.Debug("ERROR SQL: " + sql);
                ErroHandler?.Invoke("DataProvider.UpdateData", ex);
                if (throwException)
                    throw ex;
                return false;
            }
        }
    }
}
