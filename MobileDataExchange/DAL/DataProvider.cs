﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;
using MobileDataExchange.Models;
using System.Data;

namespace MobileDataExchange.DAL
{
#pragma warning disable CS1591
    public class DataProvider
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private string connectionString { get; set; }
        public DataProvider()
        {
            connectionString = WebConfigurationManager.AppSettings["ConnectionString"];
        }
        public int LogRequestSmsCode(PhoneModel phone, int limit)
        {
            int ret = 0;
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_log_request_sms_code";
                        cmd.Parameters.AddWithValue("@request_phone",phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", (phone.CountryCode.Id).ToSafeInt());
                        cmd.Parameters.AddWithValue("@limit",limit);
                        ret = int.Parse(cmd.ExecuteScalar().ToString());
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        throw;
                    }
                }
            }
            return ret;
        }
        public bool CreateClientProfile(PhoneModel phone, string bearer_token, string fcm_token)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_create_profile";
                        cmd.Parameters.AddWithValue("@phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", (phone.CountryCode.Id).ToSafeInt());
                        cmd.Parameters.AddWithValue("@bearer_token", bearer_token);
                        cmd.Parameters.AddWithValue("@fcm_token", fcm_token);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }
        public bool UpdateClientProfileBearerToken(PhoneModel phone, string bearer_token)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_update_profile_bearer_token";
                        cmd.Parameters.AddWithValue("@phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", (phone.CountryCode.Id).ToSafeInt());
                        cmd.Parameters.AddWithValue("@bearer_token", bearer_token);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }
       
        public bool UpdateClientProfile(PhoneModel phone, string first_name, string last_name)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_update_profile";
                        cmd.Parameters.AddWithValue("@phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", (phone.CountryCode?.Id).ToSafeInt());
                        cmd.Parameters.AddWithValue("@first_name", first_name);
                        cmd.Parameters.AddWithValue("@last_name", last_name);

                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }
        public bool SignoutProfile(string bearer_token)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_signout_profile";
                        cmd.Parameters.AddWithValue("@bearer_token", bearer_token);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }
        public bool UpdateClientProfileServiceAgreement(string bearer_token)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_update_profile_service_agreement";
                        cmd.Parameters.AddWithValue("@bearer_token", bearer_token);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }
        public int ValidateClientProfileToken(string token)
        {
            int ret = 0;
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_validate_profile_bearer_token";
                        cmd.Parameters.AddWithValue("@bearer_token", token);
                        ret = int.Parse(cmd.ExecuteScalar().ToString());
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        throw;
                    }
                }
            }
            return ret;
        }
        public int ValidateClientProfilePhone(PhoneModel phone)
        {
            int ret = 0;
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_validate_profile_by_phone";
                        cmd.Parameters.AddWithValue("@phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", (phone.CountryCode?.Id).ToSafeInt());
                        ret = int.Parse(cmd.ExecuteScalar().ToString());
                        cnn.Close();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        throw;
                    }
                }
            }
            return ret;
        }
        public ClientProfileModel GetClientProfileByToken(string token)
        {
            ClientProfileModel ret = null;
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_get_profile_by_token";
                        cmd.Parameters.AddWithValue("@bearer_token", token);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            ret = new ClientProfileModel
                            {
                                bearer_token = reader["bearer_token"].ToSafeString(),
                                fcm_token = reader["fcm_token"].ToSafeString(),
                                vipno = reader["vipno"].ToSafeString(),
                                first_name = reader["first_name"].ToSafeString(),
                                last_name = reader["last_name"].ToSafeString(),
                                phone_number = reader["phone_number"].ToSafeString(),
                                phone_country_code_id = reader["phone_country_code_id"].ToSafeInt(),
                                term_and_policy_agreed = reader["term_and_policy_agreed"].ToSafeBool(),
                                date_created = reader["date_created"].ToSafeDateTime(),
                                date_updated = reader["date_updated"].ToSafeDateTime()
                            };
                        }
                        else
                        {
                            return null;
                        }
                        cnn.Close();
                        return ret;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return null;
                    }
                }
            }
        }
        public ClientProfileModel GetClientProfile(PhoneModel phone)
        {
            ClientProfileModel ret = null;
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_get_profile_by_phone";
                        cmd.Parameters.AddWithValue("@phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", phone.CountryCode.Id);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            ret = new ClientProfileModel
                            {
                                bearer_token = reader["bearer_token"].ToSafeString(),
                                fcm_token = reader["fcm_token"].ToSafeString(),
                                vipno = reader["vipno"].ToSafeString(),
                                first_name = reader["first_name"].ToSafeString(),
                                last_name = reader["last_name"].ToSafeString(),
                                phone_number = reader["phone_number"].ToSafeString(),
                                phone_country_code_id = reader["phone_country_code_id"].ToSafeInt(),
                                term_and_policy_agreed = reader["term_and_policy_agreed"].ToSafeBool(),
                                date_created = reader["date_created"].ToSafeDateTime(),
                                date_updated = reader["date_updated"].ToSafeDateTime()
                            };
                        }
                        else
                        {
                            return null;
                        }
                        cnn.Close();
                        return ret;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return null;
                    }
                }
            }
        }
        public List<PlaceModel> GetSavedPlaces(PhoneModel phone)
        {
            List<PlaceModel> lst = new List<PlaceModel>();
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_get_profile_places";
                        cmd.Parameters.AddWithValue("@client_phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@client_phone_country_code_id", phone.CountryCode.Id);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            PlaceModel pl = new PlaceModel
                            {
                                Id = reader["id"].ToSafeInt(),
                                Type = reader["type"].ToSafeString(),
                                UserLabel = reader["user_label"].ToSafeString(),
                                PlaceType = reader["place_type"].ToSafeString(),
                                Name = reader["name"].ToSafeString(),
                                Point = new LocationModel
                                {
                                    Lat = reader["point_latitude"].ToSafeDouble(),
                                    Lng = reader["point_longitude"].ToSafeDouble()
                                },
                                Address = new AddressModel()
                            };
                            pl.Address.StreetNumber = reader["address_street_number"].ToSafeString();
                            pl.Address.Street = reader["address_street"].ToSafeString();
                            pl.Address.City = reader["address_city"].ToSafeString();
                            pl.Address.State = reader["address_state"].ToSafeString();
                            pl.Address.Zip = reader["address_zip"].ToSafeString();
                            pl.Address.County = reader["address_county"].ToSafeString();
                            pl.Address.Country = reader["address_country"].ToSafeString();
                            pl.Address.Location = new LocationModel
                            {
                                Lat = reader["address_location_latitude"].ToSafeDouble(),
                                Lng = reader["address_location_longitude"].ToSafeDouble()
                            };
                            pl.Airport = new AirportModel
                            {
                                iata = reader["airport_IATA"].ToSafeString(),
                                Name = reader["airport_name"].ToSafeString(),
                                Terminal = reader["airport_terminal"].ToSafeString(),
                                Flight = new FlightModel
                                {
                                    Number = reader["address_flight_number"].ToSafeString(),
                                    FromCity = reader["address_flight_from_city"].ToSafeString(),
                                    Status = reader["address_flight_status"].ToSafeString(),
                                    ArrivingTime = reader["address_flight_arriving_time"].ToSafeLong(),
                                    MeetingArea = reader["address_flight_meeting_area"].ToSafeString()
                                }
                            };
                            lst.Add(pl);
                        }
                        else
                        {
                            return lst;
                        }
                        cnn.Close();
                        return lst;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return null;
                    }
                }
            }
        }
        public List<PaymentMethodModel> GetPaymentMethods(PhoneModel phone)
        {
            List<PaymentMethodModel> lst = new List<PaymentMethodModel>();
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_get_profile_payment_methods";
                        cmd.Parameters.AddWithValue("@client_phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@client_phone_country_code_id", phone.CountryCode.Id);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            PaymentMethodModel pm = new PaymentMethodModel
                            {
                                Token = reader["token"].ToSafeString(),
                                Name = reader["name"].ToSafeString(),
                                Status = reader["status"].ToSafeString(),
                                PaymentType = reader["payment_type"].ToSafeString(),
                                BankCardType = reader["bank_card_type"].ToSafeString()
                            };
                            lst.Add(pm);
                        }
                        else
                        {
                            return lst;
                        }
                        cnn.Close();
                        return lst;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return null;
                    }
                }
            }
        }
        public bool AddPaymentMethod(PhoneModel phone, PaymentMethodModel paymentMethod)
        {
            List<PlaceModel> lst = new List<PlaceModel>();
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_create_profile_payment_method";
                        cmd.Parameters.AddWithValue("@phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", (phone.CountryCode.Id).ToSafeInt());
                        cmd.Parameters.AddWithValue("@token", paymentMethod.Token);
                        cmd.Parameters.AddWithValue("@name", paymentMethod.Name);
                        cmd.Parameters.AddWithValue("@status", paymentMethod.Status);
                        cmd.Parameters.AddWithValue("@payment_type", paymentMethod.PaymentType);
                        cmd.Parameters.AddWithValue("@bank_card_type", paymentMethod.BankCardType);
                        cmd.Parameters.AddWithValue("@active_ind", true);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }
        public bool UpdatePaymentMethod(string token, bool active_ind)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_update_profile_payment_method";
                        cmd.Parameters.AddWithValue("@token", token);
                        cmd.Parameters.AddWithValue("@active_ind", active_ind);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }

        public int AddSavedPlace(PhoneModel phone, PlaceModel place)
        {
            List<PlaceModel> lst = new List<PlaceModel>();
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_create_profile_place";
                        cmd.Parameters.AddWithValue("@phone_number", phone.Phone);
                        cmd.Parameters.AddWithValue("@phone_country_code_id", phone.CountryCode.Id);
                        cmd.Parameters.AddWithValue("@type", place.Type);
                        cmd.Parameters.AddWithValue("@user_label", place.UserLabel);
                        cmd.Parameters.AddWithValue("@place_type", place.PlaceType);
                        cmd.Parameters.AddWithValue("@name", place.Name);
                        cmd.Parameters.AddWithValue("@point_latitude", (place.Point?.Lat).ToSafeDouble());
                        cmd.Parameters.AddWithValue("@point_longitude", (place.Point?.Lng).ToSafeDouble());
                        cmd.Parameters.AddWithValue("@address_street_number", (place.Address?.StreetNumber).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_street", (place.Address?.Street).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_city", (place.Address?.City).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_state", (place.Address?.State).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_zip", (place.Address?.Zip).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_county", (place.Address?.County).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_country", (place.Address?.County).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_location_latitude", (place.Address?.Location?.Lat).ToSafeDouble());
                        cmd.Parameters.AddWithValue("@address_location_longitude", (place.Address?.Location?.Lng).ToSafeDouble());
                        cmd.Parameters.AddWithValue("@airport_IATA", (place.Airport?.iata).ToSafeString());
                        cmd.Parameters.AddWithValue("@airport_name", (place.Airport?.Name).ToSafeString());
                        cmd.Parameters.AddWithValue("@airport_terminal", (place.Airport?.Terminal).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_flight_number", (place.Airport?.Flight?.Number).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_flight_from_city", (place.Airport?.Flight?.FromCity).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_flight_status", (place.Airport?.Flight?.Status).ToSafeString());
                        cmd.Parameters.AddWithValue("@address_flight_arriving_time", (place.Airport?.Flight?.ArrivingTime).ToSafeLong());
                        cmd.Parameters.AddWithValue("@address_flight_meeting_area", (place.Airport?.Flight?.MeetingArea).ToSafeString());
                        cmd.Parameters.AddWithValue("@active_ind", true);
                        SqlParameter outputParam = new SqlParameter("@id", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        cmd.Parameters.Add(outputParam);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return (int)outputParam.Value;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return 0;
                    }
                }
            }
        }
        public bool UpdateSavedPlace(int id, bool active_ind)
        {
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cnn.Open();
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "mde_update_profile_place";
                        cmd.Parameters.AddWithValue("@id", id);
                        cmd.Parameters.AddWithValue("@active_ind", active_ind);
                        cmd.ExecuteScalar();
                        cnn.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return false;
                    }
                }
            }
        }
    }
}