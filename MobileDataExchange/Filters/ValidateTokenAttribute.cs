﻿using MobileDataExchange.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
namespace MobileDataExchange.Filters
{
#pragma warning disable CS1591
    public class ValidateTokenAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            AuthenticationHeaderValue authorizationHeader = actionContext.Request.Headers.Authorization;
            if ((authorizationHeader == null) || (authorizationHeader.Scheme.CompareTo("Bearer") != 0) || (string.IsNullOrEmpty(authorizationHeader.Parameter)))
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Missing or Invalid Bearer token");
                return;
            }
            string token = authorizationHeader.Parameter;
            if (!TokenProvider.IsTokenValid(token))
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid Bearer token");
                return;
            }
        }
    }
}