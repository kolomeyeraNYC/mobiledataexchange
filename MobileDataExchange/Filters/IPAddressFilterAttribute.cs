﻿using log4net;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace MobileDataExchange.Filters
{
#pragma warning disable CS1591
    public class IPAddressFilterAttribute : AuthorizeAttribute
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected override bool IsAuthorized(HttpActionContext context)
        {
            string ipAddressString = ((HttpContextWrapper)context.Request.Properties["MS_HttpContext"]).Request.UserHostName;
            IPAddress ipAddress = IPAddress.Parse(ipAddressString);
            IPAddress ipAllowedAddress = IPAddress.Parse(WebConfigurationManager.AppSettings["AuthorizedIP"]);
            if (ipAddress.Equals(ipAllowedAddress))
            {
                log.Debug("Request from " + ipAddress + " IP address authorized.");
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            string ipAddressString = ((HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"]).Request.UserHostName;
            IPAddress ipAddress = IPAddress.Parse(ipAddressString);
            log.Debug("Request from " + ipAddress + " unauthorized. Request from unauthorized IP address has been denied for this request.");

            base.HandleUnauthorizedRequest(actionContext);
            actionContext.Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Unauthorized,
                Content = new StringContent("Request from unauthorized IP address has been denied for this request.")
            };
        }
    }
}
