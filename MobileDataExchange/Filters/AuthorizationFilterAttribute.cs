﻿using log4net;
using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace MobileDataExchange.Filters
{
#pragma warning disable CS1591
    public class AuthorizationFilterAttribute : AuthorizeAttribute
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected override bool IsAuthorized(HttpActionContext context)
        {
            //Authorization: Basic UmlkZUV4Y2hhbmdlVXNlcjpSaWRlRXhjaGFuZ2VQYXNzd29yZA==
            //HttpContext httpContext = HttpContext.Current;
            //string authHeader = httpContext.Request.Headers["Authorization"];

            //if (context.Request.Headers.Authorization == null)
            //{
            //    return false;
            //}
            //string authHeader = context.Request.Headers.Authorization?.Parameter;

            //if (!string.IsNullOrEmpty(authHeader))
            //{
            //    string ipAddressString = ((HttpContextWrapper)context.Request.Properties["MS_HttpContext"]).Request.UserHostName;
            //    IPAddress ipAddress = IPAddress.Parse(ipAddressString);
            //    string authorizedusername = WebConfigurationManager.AppSettings["AuthorizedUserName"];
            //    string authorizedpassword = WebConfigurationManager.AppSettings["AuthorizedPassword"];

            //    Encoding encoding = Encoding.UTF8;
            //    string usernamePassword = encoding.GetString(Convert.FromBase64String(authHeader));
            //    int seperatorIndex = usernamePassword.IndexOf(':');
            //    string username = usernamePassword.Substring(0, seperatorIndex);
            //    string password = usernamePassword.Substring(seperatorIndex + 1);
            //    if (authorizedusername.Equals(username) && authorizedpassword.Equals(password))
            //    {
            //        log.Debug("Request from " + ipAddress + " Basic Authorization succeed.");
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }



            //}
            //else
            //{
            //    return false;
            //}
            return true;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            string ipAddressString = ((HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"]).Request.UserHostName;
            IPAddress ipAddress = IPAddress.Parse(ipAddressString);
            log.Debug("Request from " + ipAddress + " Basic Authorization has been denied for this request.");

            base.HandleUnauthorizedRequest(actionContext);
            actionContext.Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Unauthorized,
                Content = new StringContent("Authorization has been denied for this request.")
            };
        }
    }
}
