﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Place information </summary>
    [Serializable]
    public class PlaceModel
    {
        /// <summary>Default Constructor</summary>
        public PlaceModel()
        {

        }
        /// <summary>PlaceModel Constructor </summary>
        public PlaceModel(int id, string type, string userLabel, string placeType, string name, LocationModel point, AddressModel address, AirportModel airport)
        {
            Id = id;
            Type = type;
            UserLabel = userLabel;
            PlaceType = placeType;
            Name = name;
            Point = point;
            Address = address;
            Airport = airport;
        }

     



        /// <summary>Place Id</summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>Place Type String (enum: HOME, WORK, SAVED_PLACE, RECENT, AS_DIRECTED)</summary>
        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        /// <summary>Address user label</summary>
        [JsonProperty("userLabel")]
        [Required]
        public string UserLabel { get; set; }

        /// <summary>PlaceType String (enum: ADDRESS, LANDMARK, AIRPORT)</summary>
        [JsonProperty("placeType")]
        [Required]
        public string PlaceType { get; set; }

        /// <summary>Place Name</summary>
        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        /// <summary>Point type of Location</summary>
        [JsonProperty("point")]
        [Required]
        public LocationModel Point { get; set; }

        /// <summary>Address type of Address</summary>
        [JsonProperty("address")]
        public AddressModel Address { get; set; }

        /// <summary>Airport type of Airport</summary>
        [JsonProperty("airport")]
        public AirportModel Airport { get; set; }

    }

    
}
