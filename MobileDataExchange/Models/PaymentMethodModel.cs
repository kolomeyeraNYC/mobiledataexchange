﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MobileDataExchange.Models
{
    /// <summary>
    /// Provides Payment Method Information 
    /// </summary>
    [Serializable]
    public class PaymentMethodModel
    {
        /// <summary>Default Constructor</summary>
        public PaymentMethodModel()
        {
        }
        /// <summary>PaymentMethodModel Constructor </summary>
        public PaymentMethodModel(string token, string name, string status, string paymentType, string bankCardType)
        {
            Token = token;
            Name = name;
            Status = status;
            PaymentType = paymentType;
            BankCardType = bankCardType;
        }

        /// <summary>Payment Token information</summary>
        [Required]
        [JsonProperty("token")]
        public string Token { get; set; }



        /// <summary>Payment name</summary>
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>Payment Method Status. String of enum (VERIFIED, NOT_VERIFIED, PENDING, EXPIRED)</summary>
        [Required]
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>Payment Type. String of enum (CARD, G_PAY) </summary>
        [Required]
        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }

        /// <summary>Bank Card Type. String of enum (VISA, MASTERCARD, AMEX, DISCOVER, DINERS)  </summary>
        [Required]
        [JsonProperty("bankCardType")]
        public string BankCardType { get; set; }

    }
}
