﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Address information </summary>
    [Serializable]
    public class AddressModel
    {
        /// <summary>Default Constructor</summary>
        public AddressModel()
        {

        }

        /// <summary>AddressModel Constructor </summary>
        public AddressModel(string streetNumber, string street, string city, string state, string zip, string county, string country, LocationModel location)
        {
            StreetNumber = streetNumber;
            Street = street;
            City = city;
            State = state;
            Zip = zip;
            County = county;
            Country = country;
            Location = location;
        }


        /// <summary>SreetNumber</summary>
        [JsonProperty("streetNumber")]
        public string StreetNumber { get; set; }

        /// <summary>Sreet</summary>
        [JsonProperty("street")]
        public string Street { get; set; }

        /// <summary>City</summary>
        [JsonProperty("city")]
        public string City { get; set; }

        /// <summary>State</summary>
        [JsonProperty("state")]
        public string State { get; set; }


        /// <summary>Zip</summary>
        [JsonProperty("zip")]
        public string Zip { get; set; }

        /// <summary>County</summary>
        [JsonProperty("county")]
        public string County { get; set; }

        /// <summary>Country</summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>Location type of Location</summary>
        [JsonProperty("location")]
        public LocationModel Location { get; set; }

    }

    
}
