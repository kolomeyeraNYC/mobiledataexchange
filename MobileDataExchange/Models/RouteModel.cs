﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Route Information </summary>
    [Serializable]
    public class RouteModel
    {
        /// <summary>Default Constructor</summary>
        public RouteModel()
        {

        }
        /// <summary>RouteModel Constructor </summary>
        public RouteModel(List<PlaceModel> routePoints)
        {
            this.RoutePoints = routePoints;
        }

        /// <summary>Minimal application version</summary>
        [JsonProperty("routePoints")]
        public List<PlaceModel> RoutePoints { get; set; }
    }
}
