﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
#pragma warning disable CS1591
    public class ClientProfileModel
    {
        public ClientProfileModel()
        {

        }
        public ClientProfileModel(string bearer_token, string fcm_token, string vipno, string first_name, string last_name, string phone_number, int phone_country_code_id, bool term_and_policy_agreed, DateTime date_created, DateTime date_updated)
        {
            this.bearer_token = bearer_token;
            this.fcm_token = fcm_token;
            this.vipno = vipno;
            this.first_name = first_name;
            this.last_name = last_name;
            this.phone_number = phone_number;
            this.phone_country_code_id = phone_country_code_id;
            this.term_and_policy_agreed = term_and_policy_agreed;
            this.profile_complete = first_name.Trim().Length > 0 && last_name.Trim().Length> 0 && phone_number.Trim().Length > 0;
            this.date_created = date_created;
            this.date_updated = date_updated;
        }

        public string bearer_token { get; set; }
        public string fcm_token { get; set; }
        public string vipno { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }
        public int phone_country_code_id { get; set; }
        public bool term_and_policy_agreed { get; set; }
        public bool profile_complete { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
    }
}
