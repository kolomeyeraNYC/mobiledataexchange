﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Phone Country code information </summary>
    [Serializable]
    public class CountryCodeModel
    {
        /// <summary>Default Constructor</summary>
        public CountryCodeModel()
        {

        }
        /// <summary>CountryCodeModel Constructor</summary>
        public CountryCodeModel(int id, string name, string dialCode, string isoCode, List<int> digits)
        {
            Id = id;
            Name = name;
            DialCode = dialCode;
            IsoCode = isoCode;
            Digits = digits;
        }

        /// <summary>Country Code ID</summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>Country Code Name</summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>Country Code Dial Code</summary>
        [JsonProperty("dialCode")]
        public string DialCode { get; set; }

        /// <summary>Country Code ISO Code</summary>
        [JsonProperty("isoCode")]
        public string IsoCode { get; set; }

        /// <summary>Specifies number of digits requirement in the phone number for the country code </summary>
        [JsonProperty("digits")]
        public List<int> Digits { get; set; } = new List<int>();
    }
}
