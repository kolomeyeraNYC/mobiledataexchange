﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Bearer Token Model </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class BearerTokenModel
    {
        /// <summary>Default Constructor</summary>
        public BearerTokenModel()
        {
        }
        /// <summary>BearerTokenModel Constructor</summary>
        public BearerTokenModel(int expired_in, string token, DateTime createdDate)
        {
            Expiration = expired_in;
            Token = token;
            CreatedDate = createdDate;
        }



        /// <summary>Token expires in X days</summary>
        [JsonProperty("expires_in")]
        public int Expiration { get; set; }

        /// <summary>Token body</summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>Token Created Date</summary>
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
