﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
#pragma warning disable CS1591
    /// <summary>
    /// Provides Fare information 
    /// </summary>
    [Serializable]
    public class FareModel
    {
        /// <summary>Default Constructor</summary>
        public FareModel()
        {

        }
        /// <summary>FareModel Constructor</summary>
        public FareModel(double baseRate, string currency)
        {
            BaseRate = baseRate;
            Currency = currency;
        }

        /// <summary>Base Rate</summary>
        [JsonProperty("baseRate")]
        public double BaseRate { get; set; }

        /// <summary>Country code information. [CountryCodeModel]</summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}
