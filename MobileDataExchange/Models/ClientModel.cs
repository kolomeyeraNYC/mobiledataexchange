﻿using Newtonsoft.Json;
using System;

namespace MobileDataExchange.Models
{
    /// <summary>Provides Client information </summary>
    [Serializable]
    public class ClientModel
    {
        /// <summary>Default Constructor</summary>
        public ClientModel()
        {

        }
        /// <summary>ClientModel for Constructor</summary>
        public ClientModel(RegistrationModel registration, string firstName, string lastName, PhoneModel phone)
        {
            Registration = registration;
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
        }

        /// <summary>Registration information [RegistrationModel]</summary>
        [JsonProperty("registration")]
        public RegistrationModel Registration { get; set; }

        /// <summary>First Name</summary>
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        /// <summary>Last Name</summary>
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        /// <summary>Phone information [PhoneModel]</summary>
        [JsonProperty("phone")]
        public PhoneModel Phone { get; set; }
    }
}
