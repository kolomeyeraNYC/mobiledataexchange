﻿using log4net;
using MobileDataExchange.DAL;
using MobileDataExchange.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Configuration;
using System.Web.Http;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public class Repository
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly DataProvider dataProvider = new DataProvider();

        /// <summary>Send SMS Ferification Code</summary>
        /// <param name="phone">PhoneModel</param>
        /// <param name="request">HttpRequestMessage</param>
        public void RequestVerificationCode(PhoneModel phone, HttpRequestMessage request)
        {
            if (phone == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, "Parameter [PhoneModel] is missing"));
            }
            var ip = request.GetClientIpAddress();
            int limit = int.Parse(WebConfigurationManager.AppSettings["SMSVerificationLimitSeconds"]);
            int lastRequestSeconds = dataProvider.LogRequestSmsCode(phone, limit);
            if (lastRequestSeconds != 0 && lastRequestSeconds < limit)
            {
                HttpResponseMessage response = request.CreateErrorResponse((HttpStatusCode)429, $"Code request limit reached. Ip: {ip} Phone: {phone.Phone} Retry in {limit - lastRequestSeconds} seconds.");
                response.Headers.Add("Retry-After", $"{limit - lastRequestSeconds}");
                throw new HttpResponseException(response);
            }
            TwilioMessagingProvider tm = new TwilioMessagingProvider();
            MessageStatusModel ret = tm.SendSMSCodeRequest(phone.Phone, phone.CountryCode.IsoCode);
            if (!ret.Success)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)500, ret.Description));
            }
        }

        /// <summary>ValidateVerificationCode</summary>
        /// <param name="authrequest">AuthRequestModel arm</param>
        /// <param name="request">HttpRequestMessage request</param>
        public AuthSuccessModel ValidateVerificationCode(AuthRequestModel authrequest, HttpRequestMessage request)
        {
            if (authrequest == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, "Parameter [AuthRequest] is missing"));
            }

            if (string.IsNullOrEmpty(authrequest.VerificationCode))
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, $"The verification code is missing or invalid"));
            }
            if (authrequest.Phone == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, $"The phone information is missing or invalid"));

            }
            TwilioMessagingProvider tm = new TwilioMessagingProvider();
            MessageStatusModel ret = tm.VerifySMSCode(authrequest.Phone.Phone, authrequest.Phone.CountryCode.IsoCode, authrequest.VerificationCode);
            if (!ret.Success)
            {
                if (ret.Description == "Invalid")
                {
                    throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Invalid verification code {authrequest.VerificationCode}"));
                }
                else
                {
                    throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)500, ret.Description));
                }
            }
            string token = TokenProvider.GenerateToken(authrequest.Phone);
            ClientProfileModel clientprofile = dataProvider.GetClientProfile(authrequest.Phone);
            if (clientprofile == null)
            {
                if (!dataProvider.CreateClientProfile(authrequest.Phone, token, authrequest.FcmToken))
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);

                return new AuthSuccessModel(token, new ClientModel(null, "", "", authrequest.Phone));
            }
            else
            {
                if (!dataProvider.UpdateClientProfileBearerToken(authrequest.Phone, token))
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                return new AuthSuccessModel(token, new ClientModel(new RegistrationModel(clientprofile.term_and_policy_agreed, clientprofile.profile_complete), clientprofile.first_name, clientprofile.last_name, authrequest.Phone));
            }
            
        }

        /// <summary>Authorize</summary>
        /// <param name="authrequest">AuthRequestModel arm</param>
        /// <param name="request">HttpRequestMessage request</param>
        public AuthSuccessModel Authorize(AuthRequestModel authrequest, HttpRequestMessage request)
        {
            if (authrequest == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, "Parameter [AuthRequestModel] is missing"));
            }

            if (authrequest.Phone == null)
            {
                HttpResponseMessage response = request.CreateErrorResponse((HttpStatusCode)400, $"The phone information is missing or invalid");
                throw new HttpResponseException(response);
            }
            if (authrequest.Phone == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, $"The phone information is missing or invalid"));
            }
            ClientProfileModel clientprofile = dataProvider.GetClientProfile(authrequest.Phone);
            if (clientprofile == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Client profile not found [{authrequest.Phone.Phone}]"));
            }
            string token = TokenProvider.GenerateToken(authrequest.Phone);
            if (!dataProvider.UpdateClientProfileBearerToken(authrequest.Phone, token))
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            ClientModel cm = new ClientModel(new RegistrationModel(clientprofile.term_and_policy_agreed, clientprofile.profile_complete), clientprofile.first_name, clientprofile.last_name, authrequest.Phone);
            return new AuthSuccessModel(token, cm);
        }

        /// <summary>Signout</summary>
        public void Signout(HttpRequestMessage request)
        {
            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            if (!dataProvider.SignoutProfile(bearerToken))
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>AgreeTerms</summary>
        /// <param name="request">HttpRequestMessage request</param>
        public RegistrationModel AgreeTerms(HttpRequestMessage request)
        {
            string bearerToken = request.GetBearerToken();
            ClientProfileModel ret = dataProvider.GetClientProfileByToken(bearerToken);
            if (ret == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Invalid bearer token"));
            }
            if (!dataProvider.UpdateClientProfileServiceAgreement(bearerToken))
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            RegistrationModel rm = new RegistrationModel(true, ret.profile_complete);
            return rm;
        }

        /// <summary>ServiceAvailability</summary>
        /// <param name="request">HttpRequestMessage request</param>
        public List<ServiceTypeModel> ServiceAvailability(HttpRequestMessage request)
        {
            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            List<ServiceTypeModel> lst = new List<ServiceTypeModel>
            {
                new ServiceTypeModel(VehicleTypeEnum.SEDAN.ToString(), "Sedan", 4, 2, null, 0),
                new ServiceTypeModel(VehicleTypeEnum.SUV.ToString(), "SUV", 5, 3, null, 0),
                new ServiceTypeModel(VehicleTypeEnum.LUXURY_SEDAN.ToString(), "Luxury Sedan ", 4, 2, null, 0),
                new ServiceTypeModel(VehicleTypeEnum.WC.ToString(), "Wheelchair", 4, 2, null, 0),
                new ServiceTypeModel(VehicleTypeEnum.MINIVAN.ToString(), "Mini Van", 6, 2, null, 0)
            };
            return lst;
        }

        /// <summary>UpdateProfile</summary>
        /// <param name="client">ClientModel cm</param>
        /// /// <param name="request">HttpRequestMessage request</param>
        public ClientModel UpdateProfile(ClientModel client, HttpRequestMessage request)
        {
            if (client == null)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, "Parameter [Client] is missing"));
            }

            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            ValidateProfileByPhone(client.Phone, request);
            if (!dataProvider.UpdateClientProfile(client.Phone, client.FirstName , client.LastName))
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return client;
        }

        /// <summary>GetSavedPlaces</summary>
        /// /// <param name="request">HttpRequestMessage request</param>
        public List<PaymentMethodModel> GetPaymentMethods(HttpRequestMessage request)
        {

            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            PhoneModel phone = TokenProvider.GetPhoneFromToken(bearerToken);
            ValidateProfileByPhone(phone, request);
            return dataProvider.GetPaymentMethods(phone);
        }
        /// <summary>Add Payment Method</summary>
        /// <param name="paymentmethod">PaymentMethodModel paymentmethod</param>
        /// /// <param name="request">HttpRequestMessage request</param>
        public bool AddPaymentMethod(PaymentMethodModel paymentmethod, HttpRequestMessage request)
        {
            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            PhoneModel phone = TokenProvider.GetPhoneFromToken(bearerToken);
            ValidateProfileByPhone(phone, request);
            if (!dataProvider.AddPaymentMethod(phone, paymentmethod))
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return true;

        }
        /// <summary>
        /// Action Delete Payment Method
        /// </summary>
        /// <param name="token">token type string</param>
        /// <param name="request">request type HttpRequestMessage</param>
        public void DeletePaymentMethod(string token, HttpRequestMessage request)
        {
            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            PhoneModel phone = TokenProvider.GetPhoneFromToken(bearerToken);
            ValidateProfileByPhone(phone, request);
            if (!dataProvider.UpdatePaymentMethod(token, false))
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>GetSavedPlaces</summary>
        /// /// <param name="request">HttpRequestMessage request</param>
        public List<PlaceModel> GetSavedPlaces(HttpRequestMessage request)
        {

            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            PhoneModel phone = TokenProvider.GetPhoneFromToken(bearerToken);
            ValidateProfileByPhone(phone, request);
            return dataProvider.GetSavedPlaces(phone);
        }

        /// <summary>AddSavedPlace</summary>
        /// <param name="place">PlaceModel place</param>
        /// /// <param name="request">HttpRequestMessage request</param>
        public int AddSavedPlace(PlaceModel place, HttpRequestMessage request)
        {
            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            PhoneModel phone = TokenProvider.GetPhoneFromToken(bearerToken);
            ValidateProfileByPhone(phone, request);
            int ret = dataProvider.AddSavedPlace(phone, place);
            if (ret == 0)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            return ret;

        }

        /// <summary>
        /// Action Delete Address
        /// </summary>
        /// <param name="id">id type int</param>
        /// <param name="request">request type HttpRequestMessage</param>
        public void DeleteSavedPlace(int id, HttpRequestMessage request)
        {
            string bearerToken = request.GetBearerToken();
            ValidateProfileByBearerToken(bearerToken, request);
            PhoneModel phone = TokenProvider.GetPhoneFromToken(bearerToken);
            ValidateProfileByPhone(phone, request);
            if (!dataProvider.UpdateSavedPlace(id, false))
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
        /// <summary>GetVersions</summary>
        /// /// <param name="request">HttpRequestMessage request</param>
        public AppVersionsModel GetVersions(HttpRequestMessage request)
        {
            if ((request.Headers.UserAgent == null))
            {
                throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, "Missing or invalid User Agent request header"));
            }
            foreach (ProductInfoHeaderValue item in request.Headers.UserAgent)
            {
                if (item.ToString().ToLower().Contains("iphone") || item.ToString().ToLower().Contains("ios") || item.ToString().ToLower().Contains("ipad") || item.ToString().ToLower().Contains("ipod"))
                {
                    return new AppVersionsModel(int.Parse(WebConfigurationManager.AppSettings["iOSMinimalVersion"]), int.Parse(WebConfigurationManager.AppSettings["iOSCurrentVersion"]));
                }
                if (item.ToString().ToLower().Contains("android"))
                {
                    return new AppVersionsModel(int.Parse(WebConfigurationManager.AppSettings["AndroidMinimalVersion"]), int.Parse(WebConfigurationManager.AppSettings["AndroidCurrentVersion"]));
                };
            }
            throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid or unsupported User Agent"));
        }

        /// <summary>GetDeviceID</summary>
        /// /// <param name="request">HttpRequestMessage request</param>
        public string GetDeviceID(HttpRequestMessage request)
        {
            foreach (var header in request.Headers)
            {
                if (string.Equals(header.Key, "device-id", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(header.Key, "device_id", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(header.Key, "UID", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(header.Key, "UDID", StringComparison.OrdinalIgnoreCase)
                    )
                {
                    return header.Value.FirstOrDefault().ToSafeString();
                }
            }
            return "";
            //throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, "device-id is missing"));
        }

        private void ValidateProfileByBearerToken(string bearerToken, HttpRequestMessage request)
        {
            if (dataProvider.ValidateClientProfileToken(bearerToken) == 0)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Invalid bearer token"));
            }
        }
        private void ValidateProfileByPhone(PhoneModel phone, HttpRequestMessage request)
        {
            if (dataProvider.ValidateClientProfilePhone(phone) == 0)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)400, $"Client profile not found [{phone.Phone}]"));
            }
        }
    }
}
