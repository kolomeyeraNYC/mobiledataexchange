﻿using log4net;
using MobileDataExchange.Models;
using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Authy.V1;
using Twilio.Rest.Verify.V2.Service;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public class TwilioMessagingProvider
    {

        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string twilioAccountSid = "";
        private readonly string twilioToken = "";
        private readonly string twilioRegisteredPhoneNumber = "";
        private readonly string twilioRegisteredShortCode = "";
        private readonly string TwilioServiceSid = "";
       
        private readonly List<string> twilioShortCodeCountries = new List<string>();
        public TwilioMessagingProvider()
        {
            twilioAccountSid = WebConfigurationManager.AppSettings["TwilioAccountSid"];
            twilioToken = WebConfigurationManager.AppSettings["TwilioToken"];
            twilioRegisteredPhoneNumber = WebConfigurationManager.AppSettings["TwilioRegisteredPhoneNumber"];
            twilioRegisteredShortCode = WebConfigurationManager.AppSettings["TwilioRegisteredShortCode"];
            twilioShortCodeCountries = WebConfigurationManager.AppSettings["TwilioShortCodeCountryCodes"].Replace(" ","").Split(',').ToList();
            TwilioServiceSid = WebConfigurationManager.AppSettings["TwilioServiceSid"];
            TwilioClient.Init(twilioAccountSid, twilioToken);
        }
        private Twilio.Types.PhoneNumber GetFromPhoneNumber(string to_phone_number, string regionCode)
        {
            if (string.IsNullOrEmpty(to_phone_number) || string.IsNullOrEmpty(twilioRegisteredShortCode))
                return new Twilio.Types.PhoneNumber(PhoneNumberProvider.FormatPhoneNumber(twilioRegisteredPhoneNumber.Trim(),regionCode));

            try
            {
                PhoneNumber test_phone_number = PhoneNumberProvider.PhoneNumberUtil.Parse(to_phone_number.Trim(), regionCode);
                if (twilioShortCodeCountries.Contains(regionCode))
                {
                    return new Twilio.Types.PhoneNumber(twilioRegisteredShortCode);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return new Twilio.Types.PhoneNumber(PhoneNumberProvider.FormatPhoneNumber(twilioRegisteredPhoneNumber, regionCode));
        }
       
        public MessageStatusModel SendSMS(string to, string regionCode, string from, string msg)
        {
            MessageStatusModel status = new MessageStatusModel();
            try
            {

            Twilio.Types.PhoneNumber fromnumber = GetFromPhoneNumber(to, regionCode);
            MessageResource message= MessageResource.Create(to: to, from: fromnumber, body: msg);
            if (message == null)
            {
                log.Debug("SendSMS: Twilio Error. Failed to send twilio message..." + to);
                status.Description = "Failed to send twilio message.";
                status.Success = false;
                return status;
            }
            else
            {
                if (message.ErrorCode != null)
                {
                    log.Debug("SendSMS: Twilio Error: " + message.ErrorMessage);
                    status.Description = message.ErrorMessage;
                    status.Success = false;
                    return status;
                }
                else
                {
                    status.Success = true;
                    return status;
                }
            }
            }
            catch (Exception ex)
            {
                log.Debug(ex);
                status.Description = ex.GetErrorAllMessages();
                status.Success = false;
                return status;
            }
        }
        public MessageStatusModel SendMMS(string to, string regionCode, string from, string msg, string mms_imagepath)
        {
            MessageStatusModel status = new MessageStatusModel();
            try
            {

            var mediaUrl = new List<Uri>() { new Uri(mms_imagepath) };
            Twilio.Types.PhoneNumber fromnumber = GetFromPhoneNumber(to, regionCode);
            MessageResource message = MessageResource.Create(to: to, from: fromnumber, body: msg, mediaUrl: mediaUrl);
            if (message == null)
            {
                log.Debug("SendMMS: Twilio Error. Failed to send twilio message..." + to);
                status.Description = "Failed to send twilio message.";
                status.Success = false;
                return status;
            }
            else
            {
                if (message.ErrorCode != null)
                {
                    log.Debug("SendMMS: Twilio Error: " + message.ErrorMessage);
                    status.Description = message.ErrorMessage;
                    status.Success = false;
                    return status;
                }
                else
                {
                    status.Success = true;
                    return status;
                }
            }
            }
            catch (Exception ex)
            {
                log.Debug(ex);
                status.Description = ex.GetErrorAllMessages();
                status.Success = false;
                return status;
            }
        }

        public MessageStatusModel SendSMSCodeRequest(string to, string regionCode)
        {
            MessageStatusModel sm = new MessageStatusModel();
            try
            {
                string tonumber = PhoneNumberProvider.FormatPhoneNumber(to, regionCode);
                VerificationResource verification = VerificationResource.Create(
                to: tonumber,
                channel: "sms",
                pathServiceSid: TwilioServiceSid
                );
                sm.Success = true;
            }
            catch (Exception ex)
            {
                sm.Description = ex.Message;
                sm.Success = false;
            }
            return sm;
        }
        public MessageStatusModel VerifySMSCode(string to, string regionCode, string code)
        {
            MessageStatusModel sm = new MessageStatusModel();
            try
            {
                string tonumber = PhoneNumberProvider.FormatPhoneNumber(to, regionCode);
                VerificationCheckResource verification = VerificationCheckResource.Create(
                to: tonumber,
                code: code,
                pathServiceSid: TwilioServiceSid
                );
                if (verification.Valid == true)
                {
                    sm.Description = "Valid";
                    sm.Success = true;
                }
                else
                {
                    sm.Description = "Invalid";
                    sm.Success = false;
                }
            }
            catch (Exception ex)
            {
                sm.Description = ex.Message;
                sm.Success = false;
            }
            return sm;
        }
    }

}