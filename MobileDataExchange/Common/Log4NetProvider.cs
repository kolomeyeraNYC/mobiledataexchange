﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Repository;
using System;
using System.IO;
using System.Linq;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public class Log4NetProvider
    {
        public static void ActivateLog4Net()
        {
            try
            {
                dynamic log4NetConfigFile = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "Log4Net.config");
                XmlConfigurator.ConfigureAndWatch(log4NetConfigFile);

                var info = new DriveInfo(AppDomain.CurrentDomain.BaseDirectory);
                if (!Directory.Exists(info.Name + @"\Logs\MobileDataExchange"))
                {
                    Directory.CreateDirectory(info.Name + @"\Logs\MobileDataExchange");
                }

                ILoggerRepository repository = LogManager.GetRepository();
                IAppender[] appenders = repository.GetAppenders();
                foreach (IAppender appender in (from iAppender in appenders
                                                where iAppender is FileAppender
                                                select iAppender))
                {
                    var fileAppender = appender as FileAppender;
                    if (fileAppender != null)
                    {
                        if (fileAppender.Name == "ErrorFileAppender")
                        {
                            fileAppender.File = Path.Combine(info.Name, @"Logs\MobileDataExchange", "Error.log");
                        }
                        else if (fileAppender.Name == "InfoFileAppender")
                        {
                            fileAppender.File = Path.Combine(info.Name, @"Logs\MobileDataExchange", "Info.log");
                        }
                        else
                        {
                            fileAppender.File = Path.Combine(info.Name, @"Logs\MobileDataExchange", "Debug.log");
                        }
                        fileAppender.ActivateOptions();
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
