﻿using MobileDataExchange.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public interface IAuthService
    {
        string SecretKey { get; set; }
        bool IsTokenValid(string token);
        string GenerateToken(IAuthContainerModel model);
        IEnumerable<Claim> GetTokenClaims(string tyoken);
    }
}