﻿using log4net;
using MobileDataExchange.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public static class TokenProviderOld
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static string GenerateToken()
        {
            //byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            //byte[] key = Guid.NewGuid().ToByteArray();
            //string token = Convert.ToBase64String(time.Concat(key).ToArray());
            int expiredin = int.Parse(WebConfigurationManager.AppSettings["JWTokenExpireMinutes"]);
            BearerTokenModel token = new BearerTokenModel(expiredin, Guid.NewGuid().ToString(), DateTime.Now.ToUniversalTime());
            string output = JsonConvert.SerializeObject(token);
            return output.Base64Encode(); 
        }
      
        public static void CheckExpiration(string token, HttpRequestMessage request)
        {
            DateTime tokenDate = GetTokenDate(token, request);
            int expiredin = int.Parse(WebConfigurationManager.AppSettings["JWTokenExpireMinutes"]);
            if (tokenDate < DateTime.UtcNow.AddDays(-expiredin))
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Bearer token expired"));
            }
        }
        public static string RefreshTokenDate(string token, HttpRequestMessage request)
        {
            try
            {
                BearerTokenModel rettoken = JsonConvert.DeserializeObject<BearerTokenModel>(token.Base64Decode());
                rettoken.CreatedDate = DateTime.Now.ToUniversalTime();
                string output = JsonConvert.SerializeObject(rettoken);
                return output.Base64Encode();
            }
            catch (Exception)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Invalid bearer token"));
            }
        }
        public static DateTime GetTokenDate(string token, HttpRequestMessage request)
        {
            //byte[] data = Convert.FromBase64String(token);
            //DateTime dt = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
            //return dt;
            try
            {
                BearerTokenModel rettoken = JsonConvert.DeserializeObject<BearerTokenModel>(token.Base64Decode());
                return rettoken.CreatedDate;
            }
            catch (Exception)
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Invalid bearer token"));
            }
        }
    }
}