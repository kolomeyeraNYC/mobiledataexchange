﻿using Microsoft.IdentityModel.Tokens;
using MobileDataExchange.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public class JWTService : IAuthService
    {
#pragma warning disable CS1591
        public JWTService(string secretKey)
        {
            SecretKey = secretKey;
        }
        public string SecretKey { get; set; }

        public string GenerateToken(IAuthContainerModel model)
        {
            if (model == null || model.Claims == null || model.Claims.Length == 0)
                throw new ArgumentException("Arguments to create token are not valid");
            SecurityTokenDescriptor securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                //5259492 - 10 Years
                Subject = new ClaimsIdentity(model.Claims),
                Expires = model.ExpireMinutes > 0 ? DateTime.UtcNow.AddMinutes(Convert.ToInt32(model.ExpireMinutes)): DateTime.Now.AddMinutes(5259492),
                SigningCredentials = new SigningCredentials(GetSymmetricSecurityKey(), model.SecurityAlgorithm),
                IssuedAt = DateTime.UtcNow,
                Issuer = "CTG"
            };
            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken = jwtSecurityTokenHandler.CreateToken(securityTokenDescriptor);
            string token = jwtSecurityTokenHandler.WriteToken(securityToken);
            return token;
        }

        public IEnumerable<Claim> GetTokenClaims(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentException("Given token is null or empty");
            TokenValidationParameters tokenValidationParameters = GetTokenValidationParameters();
            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal tokenValid = jwtSecurityTokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);
                return tokenValid.Claims;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsTokenValid(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentException("Given token is null or empty");
            TokenValidationParameters tokenValidationParameters = GetTokenValidationParameters();
            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal tokenValid = jwtSecurityTokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private SecurityKey GetSymmetricSecurityKey()
        {
            byte[] symmetricKey = null;
            symmetricKey = IsBase64(SecretKey)? Convert.FromBase64String(SecretKey): Encoding.UTF8.GetBytes(SecretKey);
            return new SymmetricSecurityKey(symmetricKey);
        }
        public static bool IsBase64(String str)
        {
            if ((str.Length % 4) != 0)
            {
                return false;
            }
            try
            {
                string decoded = Encoding.UTF8.GetString(System.Convert.FromBase64String(str));
                string encoded = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(decoded));
                if (str.Equals(encoded, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            catch { }
            return false;
        }
        private TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = GetSymmetricSecurityKey(),
                ValidateIssuerSigningKey = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromMinutes(5)
                
            };
        }
    }
}