﻿using log4net;
using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public static class PhoneNumberProvider
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static PhoneNumberUtil PhoneNumberUtil = PhoneNumberUtil.GetInstance();
        public static string FormatPhoneNumber(string phoneNumber, string regionCode)
        {
           
            string returnNumber = "";
            if (string.IsNullOrWhiteSpace(phoneNumber))
                return "";

            phoneNumber = new Regex(@"\D").Replace(phoneNumber, string.Empty);
            phoneNumber = phoneNumber.Replace(" ", "").Trim();

            if (phoneNumber.Length > 15)
                return "";

            string originalPhoneNumber = phoneNumber;

            PhoneNumberUtil.ResetInstance();
            try
            {
                var testNumber = PhoneNumberUtil.Parse(phoneNumber, regionCode);
                if (!PhoneNumberUtil.IsValidNumber(testNumber) && !phoneNumber.StartsWith("+"))
                {
                    phoneNumber = "+" + phoneNumber;
                    testNumber = PhoneNumberUtil.Parse(phoneNumber, "");
                }
                if (PhoneNumberUtil.IsValidNumber(testNumber))
                {
                    returnNumber = PhoneNumberUtil.Format(testNumber, PhoneNumberFormat.E164);
                }
                else
                {
                    log.Debug("FormatPhoneNumber: " + returnNumber + " Invalid Number. Reset to original: " + originalPhoneNumber);
                    returnNumber = originalPhoneNumber;
                }
                return returnNumber;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return originalPhoneNumber;
            }
        }

        public static bool Validate(string phoneNumber, string regionCode)
        {
            try
            {
                var testNumber = PhoneNumberUtil.Parse(phoneNumber, regionCode);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Returns the region where a phone number is from. This could be used for geocoding at the region
        /// </summary>
        /// <param name="phoneNUmber">the phone number whose origin we want to know</param>
        /// <returns>The region where the phone number is from, or null if no region matches this calling code</returns>
        public static string GetRegionCodeForNumber(string phoneNUmber)
        {
            try
            {
                PhoneNumber ph = PhoneNumberUtil.Parse(phoneNUmber, ""); 
                return PhoneNumberUtil.GetRegionCodeForNumber(ph);
            }
            catch (Exception)
            {
                return "";
            }
        }
        /// <summary>
        /// Helper function to check region code is not unknown or null.
        /// </summary>
        /// <param name="regionCode">Region Code to test</param>
        /// <returns>true/false</returns>
        public static bool IsValidRegionCode(string regionCode)
        {
            try
            {
                return PhoneNumberUtil.GetSupportedRegions().Contains(regionCode);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}