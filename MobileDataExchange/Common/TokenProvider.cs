﻿using log4net;
using MobileDataExchange.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public static class TokenProvider
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static string GenerateToken(PhoneModel phone)
        {
            IAuthContainerModel model = new JWTContainerModel()
            {
                ExpireMinutes = int.Parse(WebConfigurationManager.AppSettings["JWTokenExpireMinutes"]),
                SecretKey = WebConfigurationManager.AppSettings["JWTokenSecretKey"],
                Claims = new Claim[]
                {
                    new Claim ("Phone", phone.Phone),
                    new Claim ("Phone.CountryCode.Id", phone.CountryCode.Id.ToString())
                }
            };
            IAuthService authService = new JWTService(model.SecretKey);
            return authService.GenerateToken(model);
        }
      
        public static void IsTokenValid(string token, HttpRequestMessage request)
        {
            IAuthService authService = new JWTService(WebConfigurationManager.AppSettings["JWTokenSecretKey"]);
            if (!authService.IsTokenValid(token))
            {
                throw new HttpResponseException(request.CreateErrorResponse((HttpStatusCode)401, $"Invalid or expired bearer token"));
            }
            
        }
        public static bool IsTokenValid(string token)
        {
            IAuthService authService = new JWTService(WebConfigurationManager.AppSettings["JWTokenSecretKey"]);
            return authService.IsTokenValid(token);
        }
        public static PhoneModel GetPhoneFromToken(string token)
        {
            PhoneModel pm = new PhoneModel
            {
                CountryCode = new CountryCodeModel()
            };
            IAuthService authService = new JWTService(WebConfigurationManager.AppSettings["JWTokenSecretKey"]);
            if (authService.IsTokenValid(token))
            {
                List<Claim> claims = authService.GetTokenClaims(token).ToList();
                pm.Phone= claims.FirstOrDefault(e => e.Type.Equals("Phone")).Value;
                pm.CountryCode.Id = int.Parse(claims.FirstOrDefault(e => e.Type.Equals("Phone.CountryCode.Id")).Value);
                return pm;
            }
            return null;
        }
    }
}