﻿using log4net;
using System;
using System.IO;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;

namespace MobileDataExchange.Common
{
#pragma warning disable CS1591
    public static class IOProvider
    {
        private static readonly object lockGetTempFiledName = new object();

        private static readonly object lockDeleteFile = new object();

        private static readonly object lockLogFile = new object();

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static bool DeleteDirectory(string target_dir)
        {
            try
            {
                var files = Directory.GetFiles(target_dir);
                var dirs = Directory.GetDirectories(target_dir);

                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }

                foreach (string dir in dirs)
                {
                    DeleteDirectory(dir);
                }

                Directory.Delete(target_dir, false);
                return true;
            }
            catch (Exception ex)
            {
                lock (lockLogFile)
                {
                    log.Error("DeleteDirectory", ex);
                }
            }
            return false;
        }

        public static string GetURLFileName(string url)
        {
            if (url == "")
            {
                return "";
            }
            try
            {
                var uri = new Uri(url);
                var filename = Path.GetFileName(uri.LocalPath);
                return filename;
            }
            catch (Exception ex)
            {
                lock (lockLogFile)
                {
                    log.Error("GetURLFileName", ex);
                }
                return "";
            }
        }

        public static bool EraseDirectory(string folderPath, bool recursive, string filepattern)
        {
            //Safety check for directory existence.
            if (!Directory.Exists(folderPath))
            {
                return false;
            }

            if (filepattern.Length == 0)
            {
                filepattern = "*.*";
            }

            try
            {
                foreach (string file in Directory.GetFiles(folderPath, filepattern))
                {
                    try
                    {
                        var fi = new FileInfo(file) { IsReadOnly = false };
                        fi.Delete();
                    }
                    catch
                    {
                    }

                }
            }
            catch
            {
            }

            //Iterate to sub directory only if required.
            try
            {
                if (recursive)
                {
                    foreach (string dir in Directory.GetDirectories(folderPath))
                    {
                        EraseDirectory(dir, true, filepattern);
                    }
                }
            }
            catch
            {
            }
            return true;
        }

        public static string GetTempFileName(string fType)
        {
            lock (lockGetTempFiledName)
            {
                string tempFileName = Path.GetTempFileName().Replace(".tmp", "." + fType);
                try
                {
                    if (File.Exists(tempFileName))
                    {
                        File.Delete(tempFileName);
                    }
                    return tempFileName;
                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);
                    return "";
                }
            }
        }

        public static void DeleteFile(string tmpFileName)
        {
            lock (lockDeleteFile)
            {
                try
                {
                    File.Delete(tmpFileName);
                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Returns a description of a number of bytes, in appropriate units.
        ///     e.g.
        ///     passing in 1024 will return a string "1 Kb"
        ///     passing in 1230000 will return "1.23 Mb"
        ///     Megabytes and Gigabytes are formatted to 2 decimal places.
        ///     Kilobytes are rounded to whole numbers.
        ///     If the rounding results in 0 Kb, "1 Kb" is returned, because Windows behaves like this also.
        /// </summary>
        public static string GetFileSize(long numBytes)
        {
            string fileSize;

            if (numBytes > 1073741824)
            {
                fileSize = $"{(double)numBytes / 1073741824:0.00} Gb";
            }
            else if (numBytes > 1048576)
            {
                fileSize = $"{(double)numBytes / 1048576:0.00} Mb";
            }
            else
            {
                fileSize = $"{(double)numBytes / 1024:0} Kb";
            }

            if (fileSize == "0 Kb")
            {
                fileSize = "1 Kb"; // min.							
            }
            return fileSize;
        }

        public static bool HasPathWritePermission(string path)
        {
            var writeAllow = false;
            var writeDeny = false;
            var accessControlList = Directory.GetAccessControl(path);
            var accessRules = accessControlList.GetAccessRules(true, true, typeof(SecurityIdentifier));

            foreach (FileSystemAccessRule rule in accessRules)
            {
                if ((FileSystemRights.Write & rule.FileSystemRights) != FileSystemRights.Write)
                {
                    continue;
                }

                switch (rule.AccessControlType)
                {
                    case AccessControlType.Allow:
                        writeAllow = true;
                        break;
                    case AccessControlType.Deny:
                        writeDeny = true;
                        break;
                }
            }

            return writeAllow && !writeDeny;
        }
    }
}
