﻿using log4net;
using MobileDataExchange.Common;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Xml.Serialization;

namespace System
{
#pragma warning disable CS1591
    public static class Extensions
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly IDictionary<Type, IEnumerable<PropertyInfo>> _Properties = new Dictionary<Type, IEnumerable<PropertyInfo>>();

        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static string SerializeObjectToString<T>(this T toSerialize)
        {
            return JsonConvert.SerializeObject(toSerialize);
        }

        public static string SerializeObjectToXml<T>(this T toSerialize)
        {
            string json = JsonConvert.SerializeObject(toSerialize, Formatting.Indented);

            XmlSerializer serializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, toSerialize);

                return writer.ToString();
            }
        }
        public static string GetBearerToken(this HttpRequestMessage request)
        {
            AuthenticationHeaderValue authorizationHeader = request.Headers.Authorization;
            if ((authorizationHeader == null) || (authorizationHeader.Scheme.CompareTo("Bearer") != 0) || (string.IsNullOrEmpty(authorizationHeader.Parameter)))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            return authorizationHeader.Parameter;

        }
        public static string Base64Decode(this string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static string Base64Encode(this string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                dynamic ctx = request.Properties["MS_HttpContext"];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            if (request.Properties.ContainsKey("System.ServiceModel.Channels.RemoteEndpointMessageProperty"))
            {
                dynamic remoteEndpoint = request.Properties["System.ServiceModel.Channels.RemoteEndpointMessageProperty"];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }
            return "";
        }

        public static string SanitizeURL(this string originalURL)
        {
            if (originalURL == null)
                return "";
            var ret = originalURL.Replace(" ", "%20");
            ret = ret.Replace("\"", "%22");
            ret = ret.Replace("<", "%3C");
            ret = ret.Replace(">", "%3E");
            ret = ret.Replace("#", "%23");
            ret = ret.Replace("|", "%7C");


            return ret;
        }

        public static int ValidateCCNumber(this string creditCardNumber)
        {
            const String AMEXPattern = "^3[47][0-9]{13}$";
            //const String MasterCardPattern = @"^5[1-5][0-9]{14}$";
            const String MasterCardPattern = @"^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$|^2(?:2(?:2[1-9]|[3-9]\d)|[3-6]\d\d|7(?:[01]\d|20))-?\d{4}-?\d{4}-?\d{4}$";
            const String VisaCardPattern = "^4[0-9]{12}(?:[0-9]{3})?$";
            const String DinersClubCardPattern = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
            const String enRouteCardPattern = "^(2014|2149)";
            const String DiscoverCardPattern = "^6(?:011|5[0-9]{2})[0-9]{12}$";
            const String JCBCardPattern = @"^(?:2131|1800|35\d{3})\d{11}$";
            Collections.Specialized.NameValueCollection CardPatterns = new Collections.Specialized.NameValueCollection
            {
                { "1", AMEXPattern }, //AMEX
                { "5", MasterCardPattern },//MasterCard
                { "6", VisaCardPattern }, //Visa
                { "3", DinersClubCardPattern },//DinersClub
                { "10", enRouteCardPattern },//enRoute
                { "4", DiscoverCardPattern },//Discover
                { "11", JCBCardPattern } //JCB
            };

            //// check whether input string is null or empty
            if (string.IsNullOrEmpty(creditCardNumber))
            {
                return 0;
            }

            //// 1.	Starting with the check digit double the value of every other digit
            //// 2.	If doubling of a number results in a two digits number, add up the digits to get a single digit number. This will results in eight single digit numbers
            //// 3. Get the sum of the digits
            int sumOfDigits = creditCardNumber.Where((e) => e >= '0' && e <= '9')
                            .Reverse()
                            .Select((e, i) => ((int)e - 48) * (i % 2 == 0 ? 1 : 2))
                            .Sum((e) => (e / 10) + (e % 10));

            //// If the final sum is divisible by 10, then the credit card number
            //   is valid. If it is not divisible by 10, the number is invalid.
            if (sumOfDigits % 10 != 0)
            {
                return 0;
            }

            string cardType = "";
            String cardNum = creditCardNumber.Replace(" ", "").Replace("-", "");
            try
            {
                Regex regex;
                foreach (String cardTypeName in CardPatterns.Keys)
                {
                    regex = new Regex(CardPatterns[cardTypeName]);
                    if (regex.IsMatch(cardNum))
                    {
                        cardType = cardTypeName;
                        break;
                    }
                }
            }
            catch
            {
            }
            return cardType.ToSafeInt();
        }

        public static string ToCurrency(this decimal Value)
        {
            return $"{Value:C}";
        }

        public static string ToCurrency(this double Value)
        {
            return $"{Value:C}";
        }

        public static string ToCurrencyNoSign(this decimal Value)
        {
            return $"{Value:C}";
        }

        public static string ToCurrencyNoSign(this double Value)
        {
            return $"{Value:C}";
        }

        public static string ParseNumber(this string originalString)
        {
            var regex = Regex.Match(originalString, @"\d+");
            return regex.ToString();
        }

        public static string Replace(this string originalString, string oldValue, string newValue, StringComparison comparisonType)
        {
            var startIndex = 0;
            while (true)
            {
                startIndex = originalString.IndexOf(oldValue, startIndex, comparisonType);
                if (startIndex == -1)
                {
                    break;
                }

                originalString = originalString.Substring(0, startIndex) + newValue + originalString.Substring(startIndex + oldValue.Length);

                startIndex += newValue.Length;
            }

            return originalString;
        }

        public static string ToPhoneNumber(this string obj)
        {
            if (string.IsNullOrEmpty(obj))
            {
                return "";
            }
            var value = obj;
            value = new Regex(@"\D").Replace(value, string.Empty);
            value = value.TrimStart('1');

            if (value.Length > 0 & value.Length < 4)
            {
                return $"({value.Substring(0, value.Length)})";
            }
            if (value.Length > 3 & value.Length < 7)
            {
                return $"({value.Substring(0, 3)}) {value.Substring(3, value.Length - 3)}";
            }
            if (value.Length > 6 & value.Length < 11)
            {
                return $"({value.Substring(0, 3)}) {value.Substring(3, 3)}-{value.Substring(6)}";
            }
            if (value.Length > 10)
            {
                value = value.Remove(value.Length - 1, 1);
                return $"({value.Substring(0, 3)}) {value.Substring(3, 3)}-{value.Substring(6)}";
            }
            return value;
        }

        public static string ToSafeStringTitleCase(this object obj, bool noTrim = false)
        {
            string ret = noTrim ? (obj ?? string.Empty).ToString() : (obj ?? string.Empty).ToString().Trim();
            return new CultureInfo("en-US", false).TextInfo.ToTitleCase(ret.ToLower());
        }

        public static string ToSafeString(this object obj, bool noTrim = false)
        {
            return noTrim ? (obj ?? String.Empty).ToString() : (obj ?? String.Empty).ToString().Trim();
        }

        public static string ToSafeNString(this object obj, bool noTrim = false)
        {
            //return nullable string
            return noTrim ? (obj?.ToString()) : (obj?.ToString().Trim());
        }

        public static bool Equal(this string str1, string str2, bool noTrim = false, StringComparison sc = StringComparison.CurrentCultureIgnoreCase)
        {
            if (!noTrim)
            {
                if (!string.IsNullOrEmpty(str1))
                {
                    str1 = str1.Trim();
                }

                if (!string.IsNullOrEmpty(str2))
                {
                    str2 = str2.Trim();
                }
            }
            return string.Equals(str1, str2, sc);
        }

        public static string ToString(this object obj, int maxLen = 0, bool noTrim = false, bool toProperCase = false)
        {
            string ret = "";
            ret = (obj ?? String.Empty).ToString();
            if (!noTrim)
            {
                ret = ret.Trim();
            }

            if (maxLen > 0)
            {
                ret = ret.Left(maxLen);
            }

            if (toProperCase)
            {
                ret = new CultureInfo("en-US", false).TextInfo.ToTitleCase(ret.ToLower());
            }

            return ret;
        }

        public static string ToSafeCurrencyString(this object obj, int DecimalLength = 2)
        {
            if (obj == null)
            {
                return string.Empty;
            }

            return String.Format("{0:C" + Math.Abs(DecimalLength) + "}", obj);
        }

        public static string ToSafeDecimalString(this object obj, int DecimalLength = 2)
        {
            if (obj == null)
            {
                return string.Empty;
            }

            return Convert.ToDecimal(string.Format("{0:F" + DecimalLength + "}", obj)).ToString();
        }

        public static string ToSafeDateTimeString(this object obj, string format)
        {
            var data = (obj ?? "").ToString();
            return DateTime.TryParse(data, out DateTime ret) ? ret.ToString(format) : "";
        }

        public static string ToSafeTitleCase(this object obj)
        {
            if (obj == null)
            {
                return "";
            }
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(obj.ToString().ToLower());
        }

        public static int ToSafeInt(this object obj)
        {
            var data = (obj ?? default(int)).ToString();
            return int.TryParse(data, out int ret) ? ret : 0;
        }

        public static Color ToSafeColor(this object obj)
        {
            var retcolor = default(Color);
            try
            {
                var data = (obj ?? default(int)).ToString();
                if (int.TryParse(data, out int ret))
                {
                    retcolor = Color.FromArgb(ret);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return retcolor;
        }

        public static bool IsNumeric(this object obj)
        {
            if (obj == null || obj.ToString().Length == 0)
            {
                return false;
            }
            return float.TryParse(obj.ToString(), out float ret);
        }

        public static bool IsDate(this object obj)
        {
            if (obj == null || obj.ToString().Length == 0)
            {
                return false;
            }
            if (!DateTime.TryParse(obj.ToString(), out DateTime ret))
            {
                return false;
            }

            DateTime minDateTime = new DateTime(1753, 1, 1);
            DateTime maxDateTime = new DateTime(9999, 12, 31, 23, 59, 59, 997);
            return ret >= minDateTime && ret <= maxDateTime;
        }

        public static bool IsTime(this object obj)
        {
            if (obj == null || obj.ToString().Length == 0)
            {
                return false;
            }
            return TimeSpan.TryParse(obj.ToString(), out TimeSpan dummyOutput);
        }

        public static short ToSafeShort(this object obj)
        {
            var data = (obj ?? default(short)).ToString();
            return short.TryParse(data, out short ret) ? ret : (short)0;
        }

        public static short ToSafeInt16(this object obj)
        {
            var data = (obj ?? default(short)).ToString();
            return short.TryParse(data, out short ret) ? ret : (short)0;
        }

        public static long ToSafeInt64(this object obj)
        {
            var data = (obj ?? default(short)).ToString();
            return long.TryParse(data, out long ret) ? ret : 0;
        }

        public static byte ToSafeByte(this object obj)
        {
            var data = (obj ?? default(byte)).ToString();
            switch (data.ToUpper())
            {
                case "TRUE":
                    return 1;

                case "FALSE":
                    return 0;
            }
            return byte.TryParse(data, out byte ret) ? ret : Convert.ToByte(0);
        }

        public enum BoolToStringChar
        {
            str_t_f = 0,
            str_T_F = 1,
            str_Y_N = 2,
            str_y_n = 3,
            int_1_0 = 4
        }

        public static bool ToSafeBool(this object obj)
        {
            var data = (obj ?? default(bool)).ToString();
            if (data.ToSafeDouble() > 0)
            {
                return true;
            }

            switch (data.ToUpper())
            {
                case "1":
                case "T":
                case "Y":
                case "YES":
                case "TRUE":
                case "YEA":
                case "OK":
                case "POSITIVE":
                case "SURE":
                    return true;
                case "0":
                case "F":
                case "N":
                case "NO":
                case "NIX":
                case "NOPE":
                case "NAY":
                case "NEGATIVE":
                case "FALSE":
                    return false;

                default:
                    bool ret;
                    return bool.TryParse(data, out ret) && ret;
            }
        }

        public static string ToStringChar(this bool obj, BoolToStringChar conversionType = BoolToStringChar.str_t_f)
        {
            switch (conversionType)
            {
                case BoolToStringChar.int_1_0:
                    return obj ? "1" : "0";

                case BoolToStringChar.str_t_f:
                    return obj ? "t" : "f";

                case BoolToStringChar.str_T_F:
                    return obj ? "T" : "F";

                case BoolToStringChar.str_Y_N:
                    return obj ? "Y" : "N";

                case BoolToStringChar.str_y_n:
                    return obj ? "y" : "n";

                default:
                    return obj ? "t" : "f";
            }
        }

        public static int BoolToInt(this bool obj)
        {
            return obj ? 1 : 0;
        }

        public static decimal ToSafeDecimal(this object obj)
        {
            var data = (obj ?? default(decimal)).ToString();
            return decimal.TryParse(data, out decimal ret) ? ret : (decimal.TryParse(data, NumberStyles.Currency, CultureInfo.CurrentCulture, out ret) ? ret : 0);
        }

        public static decimal ToSafeDecimal(this string obj)
        {
            return decimal.TryParse(obj, out decimal ret) ? ret : (decimal.TryParse(obj, NumberStyles.Currency, CultureInfo.CurrentCulture, out ret) ? ret : 0);
        }

        public static decimal ToSafeDecimalString(this string obj, int decimals)
        {
            return decimal.TryParse(obj, out decimal ret) ? ret : (decimal.TryParse(obj, NumberStyles.Currency, CultureInfo.CurrentCulture, out ret) ? System.Math.Round(ret, decimals) : 0);
        }
        
        public static long ToSafeLong(this object obj)
        {
            var data = (obj ?? default(long)).ToString();
            return long.TryParse(data, out long ret) ? ret : 0;
        }

        public static double ToSafeDouble(this object obj)
        {
            var data = (obj ?? default(double)).ToString();
            return double.TryParse(data, out double ret) ? ret : 0;
        }

        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        public static DateTime ToSafeDateTime(this object obj)
        {
            var data = (obj ?? default(DateTime)).ToString();
            return DateTime.TryParse(data, out DateTime ret) ? ret : default(DateTime);
        }

        public static TimeSpan ToSafeTime(this object obj)
        {
            var data = (obj ?? default(TimeSpan)).ToString();
            return TimeSpan.TryParse(data, out TimeSpan ret) ? ret : default(TimeSpan);
        }

        public static string ToStringTitleCase(this string s)
        {
            if (s == null)
            {
                return null;
            }

            var words = s.Split(' ');
            for (var i = 0; i < words.Length; i++)
            {
                if (words[i].Length == 0)
                {
                    continue;
                }

                var firstChar = char.ToUpper(words[i][0]);
                var rest = "";
                if (words[i].Length > 1)
                {
                    rest = words[i].Substring(1).ToLower();
                }
                words[i] = firstChar + rest;
            }
            return string.Join(" ", words);
        }

        public static string ToSafeSQLString(this object obj)
        {
            return (obj ?? string.Empty).ToString().Replace("'", "''").Trim();
        }

        public static string MaskCreditCard(this string text, int showLength = 4, int maskLength = -1)
        {
            if (string.IsNullOrEmpty(text) || text.Trim().Length == 0)
            {
                return "";
            }

            if (text.Length < showLength)
            {
                showLength = text.Length;
            }

            text = text.Trim();
            // Check if creditcard number needed to be masked is numbers.
            //**********1234
            if (!text.Right(showLength).IsNumeric())
            {
                return text;
            }

            if (text.Length < showLength)
            {
                return "";
            }

            if (maskLength == -1)
            {
                return new string('*', text.Length - showLength) + text.Right(showLength);
            }
            else
            {
                return new string('*', maskLength) + text.Right(showLength);
            }
        }

        public static string SanitizeFileName(this object obj)
        {
            var Fname = "";
            if (obj == null)
            {
                return Fname;
            }
            Fname = obj.ToString();
            Fname = Fname.Replace("\\", "_");
            Fname = Path.GetInvalidFileNameChars().Aggregate(Fname, (current, invalidChar) => current.Replace(invalidChar, '_'));
            return Path.GetInvalidPathChars().Aggregate(Fname, (current, invalidChar) => current.Replace(invalidChar, '_'));
        }

        public static string GetErrorAllMessages(this Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException(nameof(ex));
            }

            var sb = new StringBuilder();

            while (ex != null)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    if (!string.Equals(ex.Message, "ONE OR MORE ERRORS OCCURRED.", StringComparison.OrdinalIgnoreCase))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append("\n");
                        }

                        sb.Append(ex.Message);
                    }
                }

                ex = ex.InnerException;
            }

            return sb.ToString();
        }

        public static bool IsEmail(this object obj)
        {
            if (obj == null || obj.ToString().Length == 0)
            {
                return false;
            }
            var strIn = obj.ToString();
            try
            {
                // ReSharper disable once UnusedVariable
                var mail = new MailAddress(strIn);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static byte[] ToByteArray(this Image imageIn)
        {
            try
            {
                if (imageIn == null)
                {
                    return null;
                }
                using (var mStream = new MemoryStream())
                {
                    imageIn.Save(mStream, ImageFormat.Png);
                    return mStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return null;
        }

        public static Image FromByteArray(this byte[] arrayIn)
        {
            if (arrayIn == null)
            {
                return null;
            }
            using (var mStream = new MemoryStream(arrayIn))
            {
                return Image.FromStream(mStream);
            }
        }

        public static string Left(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) { return value; }
            maxLength = Math.Abs(maxLength);

            return value.Length <= maxLength
                    ? value
                    : value.Substring(0, maxLength)
            ;
        }

        public static string Right(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) { return value; }
            maxLength = Math.Abs(maxLength);

            return value.Length - maxLength < 0
                    ? value
                    : value.Substring(value.Length - maxLength, maxLength)
            ;
        }

        public static string Mid(this string param, int startIndex, int length)
        {
            var result = "";
            if ((param.Length > 0) && (startIndex < param.Length))
            {
                if (startIndex + length > param.Length)
                {
                    length = param.Length - startIndex;
                }

                result = param.Substring(startIndex, length);
            }
            //return the result of the operation
            return result;
        }

        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                var objType = typeof(T);
                IEnumerable<PropertyInfo> properties;

                lock (_Properties)
                {
                    if (!_Properties.TryGetValue(objType, out properties))
                    {
                        properties = objType.GetProperties().Where(property => property.CanWrite);
                        // ReSharper disable once PossibleMultipleEnumeration
                        _Properties.Add(objType, properties);
                    }
                }

                var list = new List<T>(table.Rows.Count);

                foreach (var row in table.AsEnumerable())
                {
                    var obj = new T();

                    // ReSharper disable once PossibleMultipleEnumeration
                    foreach (var prop in properties)
                    {
                        try
                        {
                            if (!prop.Name.StartsWith("__")) // system - not db fields
                            {
                                if (row[prop.Name] != DBNull.Value)
                                {
                                    if (!(row[prop.Name] is DBNull) || Nullable.GetUnderlyingType(prop.PropertyType) != null)
                                    {
                                        //var ret = Convert.ChangeType(row[prop.Name], prop.PropertyType);
                                        var ret = ChangeType(row[prop.Name], prop.PropertyType);
                                        if (prop.PropertyType.Name == "String")
                                        {
                                            prop.SetValue(obj, ((string)ret).Trim(), null);
                                        }
                                        else
                                        {
                                            prop.SetValue(obj, ret, null);
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {

                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return Enumerable.Empty<T>().ToList();
            }
        }

        private static object ChangeType(object value, Type conversion)
        {
            var t = conversion;

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return Convert.ChangeType(value, t);
        }
    }
}
