using System.Collections.ObjectModel;

namespace MobileDataExchange.Areas.HelpPage.ModelDescriptions
{
#pragma warning disable CS1591
    public class ParameterDescription
    {
        public ParameterDescription()
        {
            Annotations = new Collection<ParameterAnnotation>();
        }

        public Collection<ParameterAnnotation> Annotations { get; private set; }

        public string Documentation { get; set; }

        public string Name { get; set; }

        public ModelDescription TypeDescription { get; set; }
    }
}
