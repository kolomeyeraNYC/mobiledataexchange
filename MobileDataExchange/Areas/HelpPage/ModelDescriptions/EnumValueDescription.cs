namespace MobileDataExchange.Areas.HelpPage.ModelDescriptions
{
#pragma warning disable CS1591
    public class EnumValueDescription
    {
        public string Documentation { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}
