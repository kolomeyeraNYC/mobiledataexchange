namespace MobileDataExchange.Areas.HelpPage.ModelDescriptions
{
#pragma warning disable CS1591
    public class CollectionModelDescription : ModelDescription
    {
        public ModelDescription ElementDescription { get; set; }
    }
}
