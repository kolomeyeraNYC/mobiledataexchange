namespace MobileDataExchange.Areas.HelpPage
{
#pragma warning disable CS1591
    /// <summary>
    /// Indicates whether the sample is used for request or response
    /// </summary>
    public enum SampleDirection
    {
        Request = 0,
        Response
    }
}
